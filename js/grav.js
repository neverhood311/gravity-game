/**
*	Gravity.js
*	5/28/2015
*/

var GRAV = {};/**
*	Math.js
*	5/21/2015
*	Borrowed heavily from mrdoob/three.js
*/

var PI = 3.141592653589793;
var PI_180 = PI / 180;
var PI4 = 4 * PI;
var PI3_4 = 3 / PI4;
var TWOPI = PI * 2;

GRAV.Matrix3 = function(){
	this.elements = new Float32Array([
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	]);
};

GRAV.Matrix3.prototype = {
	constructor: GRAV.Matrix3,
	elements: undefined,
	set: function(_n11, _n12, _n13, _n21, _n22, _n23, _n31, _n32, _n33){
		var te = this.elements;
		te[ 0 ] = _n11; te[ 3 ] = _n12; te[ 6 ] = _n13;
		te[ 1 ] = _n21; te[ 4 ] = _n22; te[ 7 ] = _n23;
		te[ 2 ] = _n31; te[ 5 ] = _n32; te[ 8 ] = _n33;
		return this;
	},
	identity: function(){
		this.set(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		);
		return this;
	},
	rotate: function(_rad){
		var s = Math.sin(_rad),
		c = Math.cos(_rad);
		var e = this.elements;
		
		var e0 = e[0] * c + e[3] * s,
		e1 = e[1] * c + e[4] * s,

		e3 = e[0] * -s + e[3] * c,
		e4 = e[1] * -s + e[4] * c;
		
		e[0] = e0;
		e[1] = e1;
		e[3] = e3;
		e[4] = e4;
		
		return this;
	},
	makeRotation: function(_rad){
		var s_rad = Math.sin(_rad),
		c_rad = Math.cos(_rad);
		this.set(
			c_rad, -s_rad, 0,
			s_rad, c_rad, 0,
			0, 0, 1
		);
		return this;
	},
	translate: function(_trans){
		var e = this.elements;
		var x = _trans.x,
		y = _trans.y;
		
		//e[0] = e[0] * 1
		//e[4] = e[4] * 1
		
		e[6] = e[0] * x + e[3] * y + e[6];
		e[7] = e[1] * x + e[4] * y + e[7];
		e[8] = e[2] * x + e[5] * y + e[8];
		
		return this;
	},
	setPosition: function(_pos){
		var e = this.elements;
		e[6] = _pos.x;
		e[7] = _pos.y;
		return this;
	},
	makeTranslation: function(_pos){
		this.set(
			1, 0, _pos.x,
			0, 1, _pos.y,
			0, 0, 1
		);
		return this;		
	},
	scale: function(_sc){
		var te = this.elements;
		var x = _sc.x, y = _sc.y;
		te[0] *= x; te[3] *= y;
		te[1] *= x; te[4] *= y;
		return this;
	},
	makeScale: function(_sc){
		this.set(
			_sc.x, 0, 0,
			0, _sc.y, 0,
			0, 0, 1
		);
		return this;
	},
	multiply: function(_m){
		var me = _m.elements,
		te = this.elements;
		
		var t0 = te[0] * me[0] + te[3] * me[1] + te[6] * me[2],
			t1 = te[1] * me[0] + te[4] * me[1] + te[7] * me[2],
			t2 = te[2] * me[0] + te[5] * me[1] + te[8] * me[2],
			
			t3 = te[0] * me[3] + te[3] * me[4] + te[6] * me[5],
			t4 = te[1] * me[3] + te[4] * me[4] + te[7] * me[5],
			t5 = te[2] * me[3] + te[5] * me[4] + te[8] * me[5],
			
			t6 = te[0] * me[6] + te[3] * me[7] + te[6] * me[8],
			t7 = te[1] * me[6] + te[4] * me[7] + te[7] * me[8],
			t8 = te[2] * me[6] + te[5] * me[7] + te[8] * me[8];
		
		te[0] = t0;
		te[1] = t1;
		te[2] = t2;
		te[3] = t3;
		te[4] = t4;
		te[5] = t5;
		te[6] = t6;
		te[7] = t7;
		te[8] = t8;
		
		return this;
	},
	getInverse: function(_matrix){
		
		var te = this.elements, me = _matrix.elements;
		//ad - cb
		te[0] = me[4] * me[8] - me[5] * me[7];
		te[1] = me[7] * me[2] - me[8] * me[1];
		te[2] = me[1] * me[5] - me[2] * me[4];
		te[3] = me[6] * me[5] - me[8] * me[3];
		te[4] = me[0] * me[8] - me[2] * me[6];
		te[5] = me[3] * me[2] - me[0] * me[5];
		te[6] = me[3] * me[7] - me[4] * me[6];
		te[7] = me[6] * me[1] - me[7] * me[0];
		te[8] = me[0] * me[4] - me[1] * me[3];
		
		var det = _matrix.determinant();
		if(det === 0){
			console.warn('GRAV.Matrix3.getInverse: can\'t invert matrix; determinant is 0');
			return;
		}
		det = 1 / det;
		for(var i = 0; i < 9; i++){
			te[i] *= det;
		}
		
		return this;
	},
	determinant: function(){
		var te = this.elements;

		var a = te[ 0 ], b = te[ 1 ], c = te[ 2 ],
			d = te[ 3 ], e = te[ 4 ], f = te[ 5 ],
			g = te[ 6 ], h = te[ 7 ], i = te[ 8 ];

		return a * e * i - a * f * h - b * d * i + b * f * g + c * d * h - c * e * g;
	},
	copy: function(_m){
		var me = _m.elements;
		this.set(
			me[ 0 ], me[ 3 ], me[ 6 ],
			me[ 1 ], me[ 4 ], me[ 7 ],
			me[ 2 ], me[ 5 ], me[ 8 ]
		);
		return this;
	},
	fromArray: function(_array){
		this.elements.set(_array);
		return this;
	},
	toArray: function(){
		var te = this.elements;
		return [
			te[ 0 ], te[ 1 ], te[ 2 ],
			te[ 3 ], te[ 4 ], te[ 5 ],
			te[ 6 ], te[ 7 ], te[ 8 ]
		];
	},
	equals: function(_m){
		var me = _m.elements, te = this.elements;
		for(var i = 0; i < 9; i++){
			if(me[i] !== te[i])return false;
		}
		return true;
	},
	clone: function(){
		return new GRAV.Matrix3().fromArray(this.elements);
	}
};


GRAV.Vector2 = function(_x, _y){
	this.x = _x || 0;
	this.y = _y || 0;
};

GRAV.Vector2.prototype = {
	constructor: GRAV.Vector2,
	x: 0,
	y: 0,
	set: function(_x, _y){
		this.x = _x;
		this.y = _y;
	},
	copy: function(_v){
		this.x = _v.x;
		this.y = _v.y;
		return this;
	},
	add: function(_v){
		this.x += _v.x;
		this.y += _v.y;
		return this;
	},
	addVectors: function(_a, _b){
		this.x = _a.x + _b.x;
		this.y = _a.y + _b.y;
		return this;
	},
	sub: function(_v){
		this.x -= _v.x;
		this.y -= _v.y;
		return this;
	},
	subVectors: function(_a, _b){
		this.x = _a.x - _b.x;
		this.y = _a.y - _b.y;
		return this;
	},
	multiply: function(_v){
		this.x *= _v.x;
		this.y *= _v.y;
		return this;
	},
	divideScalar: function(_s){
		this.x /= _s;
		this.y /= _s;
		return this;
	},
	min: function(_v){
		if (this.x > _v.x){
			this.x = _v.x;
		}

		if (this.y > _v.y){
			this.y = _v.y;
		}
		return this;
	},
	max: function(_v){
		if (this.x < _v.x){
			this.x = _v.x;
		}

		if (this.y < _v.y){
			this.y = _v.y;
		}
		return this;
	},
	//_isPoint is either 1 or 0, NOT a boolean
	applyMatrix3: function(_m, _isPoint){
		var x = this.x;
		var y = this.y;
		var z = _isPoint === undefined? 1 : _isPoint;

		var e = _m.elements;

		this.x = e[ 0 ] * x + e[ 3 ] * y + e[ 6 ] * z;
		this.y = e[ 1 ] * x + e[ 4 ] * y + e[ 7 ] * z;
		//this.z = e[ 2 ] * x + e[ 5 ] * y + e[ 8 ] * z;
		return this;
	},
	dot: function(_v){
		return this.x * _v.x + this.y * _v.y;
	},
	lengthSq: function(){
		return this.x * this.x + this.y * this.y;
	},
	length: function(){
		return Math.sqrt(this.x * this.x + this.y * this.y);
	},
	normalize: function(){
		return this.divideScalar(this.length());
	},
	equals: function(_v){
		return ((_v.x === this.x) && (_v.y === this.y));
	},
	fromArray: function(_array, _offset){
		if(_offset === undefined)_offset = 0;

		this.x = _array[_offset];
		this.y = _array[_offset + 1];

		return this;
	},
	toArray: function(_array, _offset){
		if (_array === undefined)_array = [];
		if (_offset === undefined)_offset = 0;

		array[_offset] = this.x;
		array[_offset + 1] = this.y;

		return _array;
	},
	clone: function(){
		return new GRAV.Vector2(this.x, this.y);
	}
};








/**
*	Engine.js
*	5/20/2015
*/

GRAV.Engine = function(){
	//
	//
};

GRAV.Engine.prototype = {
	constructor: GRAV.Engine,
	planets: 0,
	substeps: 2,
	timescale: 1.0,
	timestep: 0.5,
	type: 'Engine',
	//real G is 0.0000000000667384
	//6.67384 * 10 ^ -11
	G: 0.1,
	maxPlanets: 0,
	capacity: 0,
	stepNum: 0,
	newPlanetCount: 0,
	destrPlanetCount: 0,
	randomStream: undefined,
	history: undefined,
	disabledHistory: undefined,
	setSubsteps: function(_num){
		this.substeps = Math.max(1, _num);
		this.setTimescale(this.timescale);
	},
	getSubsteps: function(){
		return this.substeps;
	},
	setTimescale: function(_fac){
		this.timescale = Math.max(0, _fac);
		this.timestep = this.timescale / this.substeps;
	},
	getTimescale: function(){
		return this.timescale;
	},
	step: function(){
		var i = this.substeps;
		//console.log('step');
		this.stepNum++;
		while(i--){
			//console.log('substep');
			this._substep();
		}
	},
	_substep: function(){
		console.warn('GRAV.Engine.step: Not implemented in base class.');
	},
	addPlanet: function(){
		console.warn('GRAV.Engine.addPlanet: Not implemented in base class.');
	},
	removePlanet: function(){
		console.warn('GRAV.Engine.removePlanet: Not implemented in base class.');
	},
	clear: function(){
		console.warn('GRAV.Engine.clear: Not implemented in base class.');
	},
	randomize: function(){
		console.warn('GRAV.Engine.randomize: Not implemented in base class.');
	},
	getState: function(){
		console.warn('GRAV.Engine.getState: Not implemented in base class.');
	},
	loadState: function(){
		console.warn('GRAV.Engine.loadState: Not implemented in base class.');
	},
	getTrajectory: function(){
		console.warn('GRAV.Engine.calculateTrajectory: Not implemented in base class.');
	},
	createHistory: function(){
		console.warn('GRAV.Engine.createHistory: Not implemented in base class.');
	},
	useHistory: function(){
		console.warn('GRAV.Engine.useHistory: Not implemented in base class.');
	},
	updateHistory: function(){
		console.warn('GRAV.Engine.updateHistory: Not implemented in base class.');
	},
	numPlanets: function(){
		return this.planets;
	},
	maxSize: function(){
		console.warn('GRAV.Engine.maxSize: Not implemented in base class.');
	},
	getCapacity: function(){
		return this.capacity;
	},
	planetsAtPt: function(){
		console.warn('GRAV.Engine.planetsAtPt: Not implemented in base class.');
	},
	getPlanet: function(){
		console.warn('GRAV.Engine.getPlanet: Not implemented in base class.');
	}
};

GRAV.CPUEngine = function(_params){
	GRAV.Engine.call(this);
	this.type = 'CPUEngine';
	
	_params = _params || {};
	
	this.capacity = 	_params.capacity 	|| 1024;	//this can be changed by calling this.resize() but shouldn't be greater than this.maxPlanets
	this.substeps = 	_params.substeps 	|| this.substeps;
	this.timescale = 	_params.timescale 	|| this.timescale;
	this.G = 			_params.G 			|| this.G;
	this.maxPlanets = 32767;	//this is because the collision table is an array of 16-bit ints. If more planets are required, we could switch it to 32-bit ints, giving a maximum of 2,147,483,647. We can't use Uint16 since we sometimes need to store a -1. This is unfortunate since we're wasting nearly half the integer range.
	var useHist = _params.useHistory !== undefined? _params.useHistory : true;
	
	this.randomStream = new Float32Array(this.capacity * 2);
	
	this.setTimescale(this.timescale);
	
	//X_X_xxxx: defragmented vs stationary, how many indices per block, what the entry contains
	//For example:
	//D_1_data = defragmented, each Id is for an individual planet, array entries contain data
	//D_2_data = defragmented, there are two sequential Ids that comprise a planet's data, array entries contain data
	//S_1_ID1 = stationary, each Id is for an individual planet, entries are indices into an array with a block size of 1
	this.state = {
		//the state needs to be as accurate as possible
		position: new Float64Array(this.capacity * 2),				//D_2_data
		velocity: new Float64Array(this.capacity * 2),				//D_2_data
		mass: new Float64Array(this.capacity),						//D_1_data	
		radius: new Float64Array(this.capacity),					//D_1_data
		//this would impose a hard maximum of 32767 planets
		collision: new Int16Array(this.capacity),					//S_1_ID1
		//this is the public-facing ID. It doesn't change and ids are not recycled during a single simulation run
		id: new Int16Array(this.capacity),							//D_1_data
		//Each entry contains the defragmented location of the planet with that index. It's a non-defragmented array. The capacity of this array might limit the total number of possible planets, dead or alive
		indexOf: new Int16Array(this.capacity * 2),					//S_1_ID1
		//a running list of all planets destroyed  and created since the last call to .getState()
		destroyedPlanets: new Int16Array(this.capacity),			//S_1_ID1
		newPlanets: new Int16Array(Math.ceil(this.capacity / 4))	//S_1_ID1
	};
	
	//by default, create a history
	this.createHistory();
	if(!useHist){
		this.useHistory(false);
	}
	
	//initialize all planets to non-existent
	this.clear();
};

GRAV.CPUEngine.prototype = Object.create(GRAV.Engine.prototype);
GRAV.CPUEngine.prototype.constructor = GRAV.CPUEngine;

GRAV.CPUEngine.prototype._substep = function(){
	//find all collisions
	if(this._findCollisions() > 0){
		//resolve collisions
		if(this._resolveCollisions() > 0){
			//defragment if there were collisions
			this._defragment();
		}
	}
	//update velocity
	this._updateVelocity();
	//update position
	this._updatePosition();
};

GRAV.CPUEngine.prototype._findCollisions = function(){
	var sc = this.state.collision, 
	sp = this.state.position,
	sr = this.state.radius;
	
	var curX = 0, curY = 0, curRad = 0, curRad2 = 0,
	otherX = 0, otherY = 0, otherRad = 0;
	var collision_maxRad = -1,
	collision_idx = -1,
	dist = 0,
	possible_collisions = 0;
	//for each planet find the biggest object it collides with
	for(var i = 0, len = this.last + 1, idx = i * 2, idy = idx + 1; i < len; i++, idx += 2, idy += 2){
		curRad = sr[i];
		sc[i] = -1;
		if(curRad < 0)continue;	//it's not a valid planet
		curRad2 = curRad * 2;
		curX = sp[idx];
		curY = sp[idy];
		collision_maxRad = -1;
		collision_idx = -1;
		//for each other planet
		for(var i2 = 0, idx2 = i2 * 2, idy2 = idx2 + 1; i2 < len; i2++, idx2 += 2, idy2 += 2){
			if(i === i2)continue;
			otherRad = sr[i2];
			//if the other planet is smaller, skip it. (this also handles the case that the other radius is -1)
			if(otherRad < curRad)continue;
			
			otherX = sp[idx2];
			otherY = sp[idy2];
			
			//if their bounding boxes don't overlap, they definitely won't collide
			if(Math.abs(curX - otherX) * 2 > (curRad2 + otherRad + otherRad))continue;
			if(Math.abs(curY - otherY) * 2 > (curRad2 + otherRad + otherRad))continue;

			//get the distance between the two planets
			dist = Math.sqrt(((otherX - curX) * (otherX - curX)) + ((otherY - curY) * (otherY - curY)));
			//if they don't collide, continue
			if(dist > curRad + otherRad)continue;
			//if the other planet's mass is bigger than the current largest collision
			if(otherRad > collision_maxRad){
				//if they're the same size, we'll say that the one who comes last is smaller
				if(curRad === otherRad && i2 > i)continue;
				//store the other planet's mass as the biggest collision
				collision_maxRad = otherRad;
				//store the other planet's index
				collision_idx = i2;
				possible_collisions++;
			}
		}
		//store the index of the colliding larger planet
		sc[i] = collision_idx;
	}
	
	return possible_collisions;
};

GRAV.CPUEngine.prototype._resolveCollisions = function(){
	var st = this.state,
	sp = st.position,
	sv = st.velocity,
	sm = st.mass,
	sr = st.radius,
	sc = st.collision
	si = st.id
	sio = st.indexOf,
	sdp = st.destroyedPlanets;
	var hist = this.history;
	var histLen2 = 1;
	if(hist !== undefined){
		histLen2 = hist.historyLength * 2;
	}
	var numCollisions = 0;
	var pColl = 0, pid = 0;
	var dp = this.destrPlanetCount,
	numCol = this.numCollisions,
	numPlan = this.planets;
	//for each planet
	for(var i = 0, len = this.last + 1; i < len; i++){
		pColl = 0;
		//if it collides with a bigger planet &&
		//if the bigger planet doesn't collide with anything else
		pid = sc[i];
		if(pid < 0)continue;
		//count the length of the collision chain
		while(sc[pid] >= 0){
			pColl++;
			pid = sc[pid];
		}
		//if(sc[i] >= 0 && sc[sc[i]] < 0){
		//collide if the parent has an even number of parent collisions
		if(pColl % 2 === 0){
			sdp[dp] = sio[si[i]];	//add this planet to the list of destroyed ones
			//add this planet's mass and velocity into the bigger planet
			absorbPlanet(sc[i], i);
			
			numCollisions++;
			numCol++;
			dp++;
			numPlan--;
		}
	}
	this.destrPlanetCount = dp;
	this.numCollisions = numCol;
	this.planets = numPlan;
	//clear out the collision entries
	for(var i = 0, len = this.last + 1; i < len; i++){
		sc[i] = -1;
	}
	
	//if there were collisions
	if(numCollisions > 0){
		var last = this.last;
		//update this.last
		while(sm[last] < 0 && last >= 0){
			last--;
		}
		this.last = last;
	}
	
	return numCollisions;
	
	//planet A absorbs planet B
	function absorbPlanet(_i_a, _i_b){
		var mass_a = sm[_i_a], mass_b = sm[_i_b];
		var newMass = mass_a + mass_b;
		//get the ratio of the planet masses to the combined mass
		var mass_ratio_a = mass_a / newMass,
		mass_ratio_b = 1 - mass_ratio_a;
		//get the indices into the position and velocity arrays
		var idx_a = _i_a * 2, idy_a = _i_a * 2 + 1,
		idx_b = _i_b * 2, idy_b = _i_b * 2 + 1;
		//calculate the new position
		sp[idx_a] = (sp[idx_a] * mass_ratio_a) + (sp[idx_b] * mass_ratio_b);
		sp[idy_a] = (sp[idy_a] * mass_ratio_a) + (sp[idy_b] * mass_ratio_b);
		//calculate the new velocity
		sv[idx_a] = (sv[idx_a] * mass_ratio_a) + (sv[idx_b] * mass_ratio_b);
		sv[idy_a] = (sv[idy_a] * mass_ratio_a) + (sv[idy_b] * mass_ratio_b);
		
		//store the new mass
		sm[_i_a] = newMass;
		//calculate the new radius
		sr[_i_a] = Math.pow( ( newMass * PI3_4 ) , 0.3333);
		//clear out planet B
		sm[_i_b] = -1;
		sr[_i_b] = -1;
		sio[si[_i_b]] = -1;
		si[_i_b] = -1;
		sp[idx_b] = 0;
		sp[idy_b] = 0;
		sv[idx_b] = 0;
		sv[idy_b] = 0;
		
		//if there's a history
		if(hist !== undefined){
			//inform the History that the block is now free
			hist.blockOwner[hist.blockStart[_i_b] / histLen2] = -1;
			//clear out the entry
			hist.blockStart[_i_b] = -1;
		}
	}
};

GRAV.CPUEngine.prototype._updateVelocity = function(){
	var sm = this.state.mass,
	sp = this.state.position,
	sv = this.state.velocity;
	
	//variables to be used within the loop
	var curX = 0, curY = 0, curMass = 0,
	otherX = 0, otherY = 0, otherMass = 0,
	dist2 = 0, invDist = 0;
	var vx = 0, vy = 0, ux = 0, uy = 0, acc = 0;	//more variables to be used within the loop
	var G = this.G;	//gravitational constant
	for(var i = 0, idx = i * 2, idy = idx + 1, len = this.last + 1; i < len; i++, idx += 2, idy += 2){
		curMass = sm[i];
		if(curMass < 0)continue;
		//get the current planet's info
		curX = sp[idx];
		curY = sp[idy];
		//By updating both the current planet and the other planet, 
		// I can cut the number of Math.sqrt() calls in half
		for(var i2 = i + 1, idx2 = i2 * 2, idy2 = idx2 + 1; i2 < len; i2++, idx2 += 2, idy2 +=2){
			//if it's the same planet, skip it
			//if(i2 === i)continue;	//I don't think this is necessary anymore
			
			otherMass = sm[i2];
			if(otherMass < 0)continue;	//if the planet is invalid
			
			otherX = sp[idx2];
			otherY = sp[idy2];
			
			vx = otherX - curX;
			vy = otherY - curY;
			
			dist2 = (vx * vx) + (vy * vy);
			invDist = 1 / Math.sqrt(dist2);

			//update this planet's acceleration
			ux = vx * invDist;
			uy = vy * invDist;
			acc = (G * otherMass) / dist2;
			
			sv[idx] += ux * acc;
			sv[idy] += uy * acc;
			
			//update the other planet's acceleration
			vx = curX - otherX;
			vy = curY - otherY;
			
			ux = vx * invDist;
			uy = vy * invDist;
			acc = (G * curMass) / dist2;
			
			sv[idx2] += ux * acc;
			sv[idy2] += uy * acc;
		}		
	}
};

GRAV.CPUEngine.prototype._updatePosition = function(){
	var sp = this.state.position;
	var sv = this.state.velocity;
	
	var dt = this.timestep;
	for(var i = 0, len = this.last + 1, idx = i * 2, idy = idx + 1; i < len; i++, idx += 2, idy += 2){
		sp[idx] += sv[idx] * dt;
		sp[idy] += sv[idy] * dt;
	}
};

//_out should be at least as long as the number of iterations * 2
GRAV.CPUEngine.prototype.getTrajectory = function(_x, _y, _velX, _velY, _mass, _out, _numIterations){	//once we find a good number of iterations, we can probably hard-code it
	//given a new planet (position, mass, velocity), calculate its trajectory for a few iterations
	//Make the timestep really large, like 10x the regular timestep. This is supposed to be a rough estimate, not a precise calculation
	var G = this.G;
	var sm = this.state.mass,
	sp = this.state.position;
	var fastStep = this.timestep * 1;
	var prevX = _x, prevY = _y, otherX = 0, otherY = 0, otherMass = 0,
	dist = 0, dist2 = 0;
	
	var vx = 0, vy = 0, ux = 0, uy = 0, acc = 0;	//helper variables
	for(var it = 0, itx = it * 2, ity = itx + 1; it < _numIterations; it++, itx += 2, ity += 2){
		//calculate the new velocity for this iteration
		for(var i = 0, idx = i * 2, idy = idx + 1, len = this.last + 1; i < len; i++, idx += 2, idy += 2){
			otherMass = sm[i];
			if(otherMass < 0)continue;
			
			otherX = sp[idx];
			otherY = sp[idy];
			
			dist2 = (otherX - _x) * (otherX - _x) + (otherY - _y) * (otherY - _y);
			dist = Math.sqrt(dist2);
			
			vx = otherX - _x;
			vy = otherY - _y;
			
			ux = vx / dist;
			uy = vy / dist;
			acc = (G * otherMass) / dist2;
			
			_velX += ux * acc;
			_velY += uy * acc;
		}
		//apply the velocity
		prevX += _velX * fastStep;
		prevY += _velY * fastStep;
		_out[itx] = prevX;
		_out[ity] = prevY;
	}
};

GRAV.CPUEngine.prototype.clear = function(){
	var sm = this.state.mass, sr = this.state.radius,
	sp = this.state.position, sv = this.state.velocity,
	sc = this.state.collision, si = this.state.id,
	sio = this.state.indexOf, sdp = this.state.destroyedPlanets,
	snp = this.state.newPlanets;
	
	//these ones have length this.capacity
	for(var i = 0, len = sm.length; i < len; i++){
		sm[i] = -1;
		sr[i] = -1;
		sc[i] = -1;
		si[i] = -1;
		sdp[i] = -1;
	}
	
	for(var i = 0, len = snp.length; i < len; i++){
		snp[i] = -1;
	}
	
	//these ones have length this.capacity * 2
	for(var i = 0, len = sp.length; i < len; i++){
		sp[i] = 0;
		sv[i] = 0;
	}
	
	for(var i = 0, len = sio.length; i < len; i++){
		sio[i] = -1;
	}
	
	if(this.history !== undefined){
		this.history.clear();
	}
	
	this.next = 0;
	this.nextID = 0;
	this.last = -1;
	
	this.numCollisions = 0;
	this.newPlanetCount = 0;
	this.destrPlanetCount = 0;
	this.planets = 0;
	this.defragCount = 0;
};

GRAV.CPUEngine.prototype.randomize = function(_params){
	_params = _params || {};
	
	var num = _params.num || Math.floor(this.capacity * 0.5);
	var xbound = _params.xbound || 800;
	var ybound = _params.ybound || 800;
	var minMass = _params.minMass || 5;
	var maxMass = _params.maxMass || 400;
	var maxVel = _params.maxVel || 0;
	var elliptical = _params.elliptical || false;
	
	this.clear();
	
	var rand = this.randomStream;
	if(elliptical){
		var xrange = xbound * 0.5;
		var yrange = ybound * 0.5;
		var angle = 0, radius = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1; i < num; i++, idx += 2, idy += 2){
			angle = Math.random() * TWOPI;
			radius = Math.sqrt(Math.random());
			rand[idx] = Math.cos(angle) * radius * xrange;
			rand[idy] = Math.sin(angle) * radius * yrange;
		}
	}
	else{
		for(var i = 0, idx = i * 2, idy = idx + 1; i < num; i++, idx += 2, idy += 2){
			rand[idx] = Math.random() * xbound - (xbound * 0.5);
			rand[idy] = Math.random() * ybound - (ybound * 0.5);
		}
	}
	
	var sp = this.state.position,
	sm = this.state.mass,
	sr = this.state.radius,
	sv = this.state.velocity
	si = this.state.id,
	sio = this.state.indexOf;
	
	for(var i = 0, idx = i * 2, idy = idx + 1; i < num; i++, idx += 2, idy += 2){
		sm[i] = Math.random() * (maxMass - minMass) + minMass;
		sr[i] = Math.pow( ( sm[i] * PI3_4 ), 0.3333);
		sp[idx] = rand[idx];
		sp[idy] = rand[idy];
		sv[idx] = Math.random() * maxVel * 2 - maxVel;
		sv[idy] = Math.random() * maxVel * 2 - maxVel;
		si[i] = this.nextID++;
		sio[i] = i;
	}
	this.next = num;
	this.last = num - 1;
	
	//initialize the history
	if(this.history !== undefined){
		this.history.fill(sp, num);
	}
	
	this.planets = num;
};

GRAV.CPUEngine.prototype._defragment = function(){
	var hasHistory = this.history !== undefined;
	var bs = undefined, ts = undefined, bo = undefined,
	histLen = 0;
	if(hasHistory){
		bs = this.history.blockStart;
		ts = this.history.tailStart;
		bo = this.history.blockOwner;
		histLen2 = this.history.historyLength * 2;
	}
	//restructure the contents of the state arrays so that there are no gaps
	var sm = this.state.mass,
	sp = this.state.position,
	sv = this.state.velocity,
	sr = this.state.radius,
	si = this.state.id,
	sio = this.state.indexOf;
	var next = 0, cap = this.capacity;
	//find the first available spot
	while(sm[next] > -1 && next < cap){
		next++;
	}
	
	for(var i = next, len = this.last + 1, idx = i * 2, idy = idx + 1; i < len; i++, idx += 2, idy += 2){
		//if there's a planet here
		if(sm[i] > 0){
			//move it down to the next slot
			sm[next] = sm[i];
			sr[next] = sr[i];
			si[next] = si[i];
			sp[next * 2] = sp[idx];
			sp[next * 2 + 1] = sp[idy];
			sv[next * 2] = sv[idx];
			sv[next * 2 + 1] = sv[idy];
			
			//update indexOf
			sio[si[next]] = next;
			
			//clear the old slots
			sm[i] = -1;
			sr[i] = -1;
			si[i] = -1;
			sp[idx] = 0;
			sp[idy] = 0;
			sv[idx] = 0;
			sv[idy] = 0;
			
			//if there is a history, defragment its blockStart, blockOwner, and tailStart arrays
			if(hasHistory){
				bo[bs[i] / histLen2] = next;
				bs[next] = bs[i];
				ts[next] = ts[i];
				
				bs[i] = -1;
				ts[i] = -1;
			}
			//update 'next'
			while(sm[next] > -1 && next < cap){
				next++;
			}
		}
	}
	this.last = next - 1;
	this.next = next;
	if(hasHistory){
		this.history.last = this.last;
	}
	this.defragCount++;
};

//maybe call resize if the arrays are not being adequately filled, like if there are fewer than 1/16th the elements being used
GRAV.CPUEngine.prototype._resize = function(_newMax){
	//TODO
	console.log('resized');
	//should we even allow this? This would make some things kinda hard
};

GRAV.CPUEngine.prototype.addPlanet = function(_x, _y, _velx, _vely, _mass){
	if(this.planets === this.capacity){
		console.log('The simulation has reached maximum capacity!');
		return;
	}
	//call resize if necessary
	/*if(this.planets >= this.capacity - 1){
		this._resize();
	}*/
	//put it in the next empty slot
	var st = this.state;
	var n = this.next;
	st.mass[n] = _mass;
	st.radius[n] = Math.pow( ( _mass * PI3_4 ) , 0.3333);
	st.position[n * 2] = _x;
	st.position[n * 2 + 1] = _y;
	st.velocity[n * 2] = _velx;
	st.velocity[n * 2 + 1] = _vely;
	st.id[n] = this.nextID++;
	st.indexOf[st.id[n]] = n;
	st.newPlanets[this.newPlanetCount] = st.id[n];
	//update the history, if any
	if(this.history !== undefined){
		this.history.addPlanet(_x, _y, n);
	}
	var cap = this.capacity;
	//find the new next empty slot
	while(st.mass[n % cap] > 0){
		n++;
	}
	this.next = n;
	this.planets++;
	this.newPlanetCount++;
	//update this.last
	this.last = n - 1;
};

GRAV.CPUEngine.prototype.removePlanet = function(_pid){	//_id is the user-facing ID, not the planet's index in the state.mass array
	var st = this.state;
	var id = st.indexOf[_pid];
	if(id === -1)return;
	
	st.mass[id] = -1;
	st.radius[id] = -1;
	st.indexOf[_pid] = -1;
	st.id[id] = -1;
	st.position[id * 2] = 0;
	st.position[id * 2 + 1] = 0;
	st.velocity[id * 2] = 0;
	st.velocity[id * 2 + 1] = 0;
	
	var hist = this.history;
	
	if(hist !== undefined){
		hist.removePlanet(id);
		/*hist.blockOwner[hist.blockStart[id] / (hist.historyLength << 1)] = -1;
		hist.blockStart[id] = -1;*/
	}
	
	st.destroyedPlanets[this.destrPlanetCount] = _pid;
	this.destrPlanetCount++;
	this.planets--;
	this._defragment();
};

GRAV.CPUEngine.prototype.createHistory = function(_params){
	_params = _params || {};
	this.history = new GRAV.HistoryManager({
		capacity: this.capacity,
		historyLength: _params.historyLength,
		skip: _params.skip
	});
};

//returns whether or not the setting was actually changed
GRAV.CPUEngine.prototype.useHistory = function(_use){
	if(_use === undefined)_use = true;
	
	//if this is already enabled/disabled, don't do it again
	if(_use == (this.history !== undefined))return false;
	
	if(_use){
		this.history = this.disabledHistory;
		this.disabledHistory = undefined;
	}
	else{
		this.disabledHistory = this.history;
		this.history = undefined;
	}
	return true;
};

GRAV.CPUEngine.prototype.updateHistory = function(){
	var hist = this.history;
	if(hist == undefined || !hist.needsUpdate(this.stepNum))return;
	var ts = hist.tailStart,
	bs = hist.blockStart,
	posHist = hist.history,
	update = hist.ptsNeedUpdate,
	histLen2 = hist.historyLength * 2;
	var sp = this.state.position,
	sm = this.state.mass;
	//add latest positions to the position history
	//update the history's frame number
	var curTail = 0, newTail = 0, curBlock = 0;
	
	for(var i = 0, idx = i * 2, idy = idx + 1, len = this.last + 1, count = 0; i < len; i++, idx += 2, idy += 2){
		if(sm[i] < 0)continue;
		//get the block position
		curBlock = bs[i];
		//get the current tail position
		curTail = ts[i];
		//find the new tail position
		newTail = ((((curTail - 2) % histLen2) + histLen2) % histLen2);	//this weirdness ensures it's positive
		//store the new position
		posHist[curBlock + newTail] = sp[idx];
		posHist[curBlock + newTail + 1] = sp[idy];
		//set the update flag
		//update[(curBlock >> 1) + (newTail >> 1)] = 1;	//this has a bit-shift since it's half the size
		update[count] = curBlock + newTail;
		//store the new tail position
		ts[i] = newTail;
		count++;
		hist.needsUpdateLatest = true;
	}
};

GRAV.CPUEngine.prototype.maxSize = function(){
	return this.maxPlanets;
};

//make sure the arrays you pass in are big enough to fit all the planets!!!
GRAV.CPUEngine.prototype.getState = function(_state){
	var ts = this.state;
	//copy the planet positions and their radii into the arrays provided by the caller
	_state.positions.set(ts.position);
	_state.radii.set(ts.radius);
	_state.velocities.set(ts.velocity);
	_state.numPlanets = this.planets;
	_state.matrixApplied = false;
	
	//include the IDs of destroyed planets and new planets
	_state.created.set(ts.newPlanets);
	_state.destroyed.set(ts.destroyedPlanets);
	//clear out these arrays
	for(var i = 0, len = this.newPlanetCount; i < len; i++){
		ts.newPlanets[i] = -1;
	}
	this.newPlanetCount = 0;
	for(var i = 0, len = this.destrPlanetCount; i < len; i++){
		ts.destroyedPlanets[i] = -1;
	}
	this.destrPlanetCount = 0;
	
	//return the number of planets in the array
	return this.planets;
};

GRAV.CPUEngine.prototype.loadState = function(_state){
	//TODO
};

GRAV.CPUEngine.prototype.planetsAtPt = function(_x, _y, _out){
	var count = 0, outIdx = 0, outSize = _out.length;
	var sp = this.state.position,
	sr = this.state.radius,
	sm = this.state.mass,
	si = this.state.id;
	var posx = 0, posy = 0, rad = 0, dist = 0;
	//find all the planets who encompass the point [_x, _y]
	for(var i = 0, idx = i * 2, idy = idx + 1, len = this.last + 1; i < len; i++, idx += 2, idy += 2){
		if(sm[i] < 0)continue;
		posx = sp[idx];
		posy = sp[idy];
		rad = sr[i];
		
		if(_x < posx - rad)continue;	//too far left
		if(_x > posx + rad)continue;	//too far right
		if(_y < posy - rad)continue;	//too far up
		if(_y > posy + rad)continue;	//too far down
		
		//get the actual distance
		dist = Math.sqrt(((_x - posx) * (_x - posx)) + ((_y - posy) * (_y - posy)));
		if(dist > rad)continue;
		
		//we've found one
		if(outIdx < outSize){
			_out[outIdx] = si[i];
			outIdx++;
		}
		count++;
	}
	//return the number selected
	return count;
};

GRAV.CPUEngine.prototype.getPlanet = function(_id, _out){
	_out = _out || {};
	var st = this.state
	sm = st.mass,
	si = st.id,
	sio = st.indexOf;
	var idx = sio[_id];
	if(idx !== -1 && !isNaN(idx)){
		_out.x = st.position[idx * 2];
		_out.y = st.position[idx * 2 + 1];
		_out.velx = st.velocity[idx * 2];
		_out.vely = st.velocity[idx * 2 + 1];
		_out.mass = sm[idx];
		_out.radius = st.radius[idx];
	}
	
	//This could be a backup method if the planet was not found and the number of planets that have ever existed has exceeded the limit
	//find the planet
	/*for(var i = 0, len = this.last + 1; i < len; i++){
		if(sm[i] < 0)continue;
		if(si[i] === _id){
			_out.x = st.position[i * 2];
			_out.y = st.position[i * 2 + 1];
			_out.velx = st.velocity[i * 2];
			_out.vely = st.velocity[i * 2 + 1];
			_out.mass = sm[i];
			_out.radius = st.radius[i];
			return _out;
		}
	}*/
	return _out;
};

//Debugging functions
GRAV.CPUEngine.prototype.distanceBetween = function(_a, _b){
	var x1 = this.state.position[_a * 2];
	var y1 = this.state.position[_a * 2 + 1];
	
	var x2 = this.state.position[_b * 2];
	var y2 = this.state.position[_b * 2 + 1];
	var one = x2 - x1;
	var two = y2 - y1;
	
	return Math.sqrt((one * one) + (two * two));
};

GRAV.CPUEngine.prototype.totalMass = function(){
	var mass = 0;
	var sm = this.state.mass;
	for(var i = 0; i < this.capacity; i++){
		if(sm[i] > 0){
			mass += sm[i];
		}
	}
	return mass;
};

GRAV.CPUEngine.prototype.totalEnergy = function(){
	//E = 0.5 * m * v^2
	var sm = this.state.mass,
	sv = this.state.velocity;
	var totalEnergy = 0,
	curEnergy = 0,
	curVel2 = 0;
	for(var i = 0, idx = i * 2, idy = idx + 1, len = this.last + 1; i < len; i++, idx += 2, idy += 2){
		if(sm[i] < 0)continue;
		curVel2 = (sv[idx] * sv[idx]) + (sv[idy] * sv[idy]);
		curEnergy = 0.5 * sm[i] * curVel2;
		totalEnergy += curEnergy;
	}
	return totalEnergy;
};

GRAV.CPUEngine.prototype.test = function(_params){
	_params = _params || {};
	
	var iterations = _params.iterations || 1000;
	var trials = _params.trials || 20;
	var useHistory = _params.useHistory === undefined ? false : _params.useHistory;
	
	var trialRuns = new Float64Array(trials);
	var start = 0, end = 0, i = trials;
	var randSettings = {
		num: 512,
		xbound: 800,
		ybound: 800,
		minMass: 5,
		maxMass: 400,
		maxVel: 0,
		elliptical: false
	};
	var historyBackup = this.history;
	var changed = this.useHistory(useHistory);
		
	for(var i = 0; i < trials; i++){
		this.randomize(randSettings);
		start = performance.now();
		for(var i2 = 0; i2 < iterations; i2++){
			this.step();
		}
		end = performance.now();
		trialRuns[i] = end - start;
	}
	if(changed){
		this.useHistory(!useHistory);
	}
	
	var sum = 0;
	for(var i = 0; i < trials; i++){
		sum += trialRuns[i];
	}
	var average = sum / trials;
	console.log('' + average + ' ms');
	//alert('done');
};












/**
*	State.js
*	5/28/2015
*	A class for transferring the current state of the simulation
*/

GRAV.State = function(_size){
	this.size = _size || 1024;
	this.positions = new Float32Array(this.size * 2);	//this doesn't have to be as accurate as the engine
	this.velocities = new Float32Array(this.size * 2);
	this.radii = new Float32Array(this.size);
	this.visibility = new Uint8Array(this.size);	//the value range is 0 to 15, so 8 bits is almost overkill
	this.created = new Int16Array(Math.ceil(this.size / 4));
	this.destroyed = new Int16Array(this.size);
};

GRAV.State.prototype = {
	constructor: GRAV.State,
	size: 1024,
	positions: undefined,
	velocities: undefined,
	radii: undefined,
	created: undefined,
	destroyed: undefined,
	numPlanets: 0,
	matrixApplied: false,
	setSize: function(_size){
		if(_size === undefined || _size === this.size){
			return;
		}
		this.size = _size;
		//TODO: resize each array
	},
	loadFromJSONstr: function(_jsonStr){
		//TODO
	},
	toJSONstr: function(){
		//TODO
	}
};

/**
*	Camera.js
*	5/21/2015
*/

GRAV.Camera = function(){
	this.position = new GRAV.Vector2();
	this.negPosition = new GRAV.Vector2();
	this.scale = new GRAV.Vector2(1,1);
	this.matrixWorld = new GRAV.Matrix3();
	this.invMatrixWorld = new GRAV.Matrix3();
	this.viewport = new GRAV.Vector2();
	this.setSize(512, 512);
};

GRAV.Camera.prototype = {
	constructor: GRAV.Camera,
	matrixWorld: undefined,
	invMatrixWorld: undefined,
	position: undefined,
	negPosition: undefined,
	scale: undefined,
	rotation: 0,	//in radians
	h_viewport: undefined,
	width: 0,
	height: 0,
	h_width: 0,
	h_height: 0,
	changed: true,
	setSize: function(_w, _h, _update){
		if(this.h_viewport === undefined){
			this.h_viewport = new GRAV.Vector2();
		}
		this.width = _w;
		this.h_width = _w * 0.5;
		this.height = _h;
		this.h_height = _h * 0.5;
		this.h_viewport.set(this.h_width, this.h_height);
		if(_update === false)return;
		this.updateMatrix();
	},
	setPosition: function(_x, _y, _update){
		this.position.set(_x, _y);
		this.negPosition.set(-_x, -_y);
		if(_update === false)return;
		this.updateMatrix();
	},
	setScale: function(_sc, _update){
		this.scale.set(_sc, _sc);
		if(_update === false)return;
		this.updateMatrix();
	},
	setScale2D: function(_x, _y, _update){
		this.scale.set(_x, _y);
		if(_update === false)return;
		this.updateMatrix();
	},
	setRotation: function(_rad, _update){
		this.rotation = _rad;
		if(_update === false)return;
		this.updateMatrix();
	},
	setRotationDeg: function(_deg, _update){
		this.setRotation(_deg * PI_180, _update);
	},
	updateMatrix: function(){
		//Translation
		//Scale
		//Rotation
		this.matrixWorld.identity().setPosition(this.h_viewport).translate(this.position).translate(this.negPosition).scale(this.scale).rotate(this.rotation).translate(this.position);
		
		this.invMatrixWorld.getInverse(this.matrixWorld);
		
		this.changed = true;
	},
	//pass in the radii so that we can skip any invalid planets
	applyMatrixToState: function(_state){
		if(_state.matrixApplied)return;
		
		var pos = _state.positions,
		radii = _state.radii,
		num = _state.numPlanets;
		var e = this.matrixWorld.elements,
		sc = this.scale.x,
		posx = 0, posy = 0,
		size = _state.size;
		for(var i = 0, idx = i * 2, idy = idx + 1, count = 0; count < num && i < size; i++, idx += 2, idy += 2){
			//if it's a real planet
			if(radii[i] > 0){
				//apply the matrix to the position
				posx = e[0] * pos[idx] + e[3] * pos[idy] + e[6];
				posy = e[1] * pos[idx] + e[4] * pos[idy] + e[7];
				pos[idx] = posx;
				pos[idy] = posy;
				
				//apply the scale to the radius
				radii[i] *= sc;
				
				count++;
			}
		}
		_state.matrixApplied = true;
	},
	applyMatrixToHistory: function(_history){
		var e = this.matrixWorld.elements,
		tpts = _history.transformedPts,
		posHist = _history.history,
		pnu = _history.ptsNeedUpdate;
		var posx = 0, posy = 0;
		if(this.changed || _history.needsUpdateAll){
			//update everything
			//only update the entries that need updating
			var start = -1,
			histLen = _history.historyLength,
			bs = _history.blockStart;
			for(var i = 0, len = _history.last + 1; i < len; i++){
				start = bs[i];
				if(start < 0)continue;
				//loop through the history for this planet
				for(var i2 = 0, idx2 = start, idy2 = idx2 + 1; i2 < histLen; i2++, idx2 += 2, idy2 += 2){
					//update the point
					tpts[idx2] = e[0] * posHist[idx2] + e[3] * posHist[idy2] + e[6];
					tpts[idy2] = e[1] * posHist[idx2] + e[4] * posHist[idy2] + e[7];
				}
				//clear out the ptsNeedUpdate array as you go
				pnu[i] = -1;
			}
			this.changed = false;
			_history.needsUpdateLatest = false;
			_history.needsUpdateAll = false;
		}
		else if(_history.needsUpdateLatest){
			var idx = 0, idy = 0, posx = 0, posy = 0;
			for(var i = 0, len = _history.last + 1; i < len; i++){
				//get the index of the next point that needs updating
				idx = pnu[i];
				if(idx < 0)continue;	//this shouldn't ever happen
				idy = idx + 1;
				//transform the point
				//store it in the transformed points array
				tpts[idx] = e[0] * posHist[idx] + e[3] * posHist[idy] + e[6];
				tpts[idy] = e[1] * posHist[idx] + e[4] * posHist[idy] + e[7];
				//clear out the flagged index
				pnu[i] = -1;
			}
			_history.needsUpdateLatest = false;
		}
	},
	//pass in an array of positions and radii
	//pass in an array for the results to be stored
	//optionally pass in the number of indices to check
	//performs the visibility check on each planet
	/*values are encoded in the following manner:
	1010 | 0010 | 0110
	------------------
	1000 | 0000 | 0100
	------------------
	1001 | 0001 | 0101
	Where the region labelled 0000 is the viewport and the other regions are outside the viewport
	
	If a planet is invalid, the value is set to 15 (0b1111)
	*/
	checkVisibility: function(_state){
		var num = _state.numPlanets,
		positions = _state.positions,
		radii = _state.radii,
		visibility = _state.visibility;
		var numVisible = 0;
		
		var width = this.width, height = this.height;
		var posx = 0, posy = 0, radius = 0, visible = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = radii.length; i < len && count < num; i++, idx += 2, idy += 2){
			visibility[i] = 15; //0b1111
			radius = radii[i];
			if(radius < 0)continue;
			
			count++;
			
			posx = positions[idx];
			posy = positions[idy];
			
			visible = 0; //0b0000
			//check for visibility
			if(posx + radius < 0)
				visible = visible | 8; //0b1000
			if(posx - radius > width)
				visible = visible | 4; //0b0100
			if(posy + radius < 0)
				visible = visible | 2; //0b0010
			if(posy - radius > height)
				visible = visible | 1; //0b0001
			
			visibility[i] = visible;
			if(visible === 0) //0b0000
				numVisible++;
		}
		return numVisible;
	},
	//project a world-space point to screen space 
	project: function(_pos){
		//TODO
	},
	//project a screen-space point to world space
	unprojectVector: function(_pos){
		//un-shift by the viewport dimensions
		//un-translate
		//un-scale
		//un-rotate
		_pos.applyMatrix3(this.invMatrixWorld);
	}
};


/*
Transform from world space to camera space:
Rotate
Scale
Translate

Transform from camera space to screen space:
Shift by the viewport dimensions



*/















/**
*	CameraControls.js
*	5/27/2015
*/

GRAV.CameraControls = function(_cam, _canvas){
	this.camera = _cam;
	this.canvas = _canvas;
	
	this.enabled = true;
	
	this.position = new GRAV.Vector2();
	this.rotation = 0;	//stored in degrees
	
	this.zoom = 1.0;
	
	this.noZoom = false;
	this.zoomSpeed = 1.0;
	
	this.minZoom = 0.0005;
	this.maxZoom = 100;
	
	this.noRotate = false;
	this.rotateSpeed = 1.0;
	
	this.noPan = false;
	
	var MOUSE = {LEFT: 0, MIDDLE: 1, RIGHT: 2};
	this.mouseButtons = { PAN: MOUSE.LEFT, ROTATE: MOUSE.RIGHT };
	
	//internals
	var scope = this;
	
	this.position0 = this.position.clone();
	
	var panStart = new GRAV.Vector2();
	var panEnd = new GRAV.Vector2();
	var panDelta = new GRAV.Vector2();
	var panOffset = new GRAV.Vector2();
	
	var rotateStart = 0;
	var rotateEnd = 0;
	var rotateDelta = 0;
	var STATES = {NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2};
	var state = STATES.NONE;
	
	//functions
	this.pan = function(_deltaX, _deltaY){
		if(this.noPan)return;
		
		panOffset.set(_deltaX, _deltaY);
		//apply the zoom factor and rotation
		panOffset.x /= this.zoom;
		panOffset.y /= this.zoom;
		var c = Math.cos(this.rotation * PI_180),
		s = Math.sin(this.rotation * PI_180);
		var panx = panOffset.x * c + panOffset.y * s;
		var pany = panOffset.x * -s + panOffset.y * c;
		
		panOffset.set(panx, pany);
		
		//panOffset.applyMatrix3(this.camera.invMatrixWorld, 0);
		
		this.position.add(panOffset);
	};
	this.rotate = function(_num){
		if(this.noRotate)return;
		this.rotation += _num * 0.333;
	};
	this.zoomIn = function(){
		this.zoom *= 1.1 * this.zoomSpeed;
		this.zoom = Math.min(this.zoom, this.maxZoom);
	};
	this.zoomOut = function(){
		this.zoom *= 0.9 * this.zoomSpeed;
		this.zoom = Math.max(this.zoom, this.minZoom);
	};
	this.reset = function(){
		//state = STATES.NONE;
		this.position.copy(this.position0);
		this.rotation = 0;
		this.zoom = 1.0;
		this.update();
	};
	this.update = function(){
		//apply the position, rotation, and zoom to the camera
		this.camera.setPosition(this.position.x, this.position.y, false);
		this.camera.setScale(this.zoom, false);
		this.camera.setRotationDeg(this.rotation, false);
		this.camera.updateMatrix();
	};
	
	function onMouseDown(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		switch(event.button){
			case scope.mouseButtons.PAN:
				state = STATES.PAN;
				panStart.set(event.clientX, event.clientY);
				break;
			case scope.mouseButtons.ROTATE:
				state = STATES.ROTATE;
				rotateStart = event.clientY;
				break;
		}
		
		document.addEventListener('mousemove', onMouseMove, false);
		document.addEventListener('mouseup', onMouseUp, false);
	}
	
	function onMouseWheel(event){
		if(!scope.enabled || scope.noZoom)return;
		
		event.preventDefault();
		event.stopPropagation();
		
		var delta = 0;
		
		if(event.wheelDelta !== undefined){
			delta = event.wheelDelta;
		}
		else if(event.detail !== undefined){
			delta = -event.detail;
		}
		
		if(delta > 0){
			scope.zoomIn();
		}
		else{
			scope.zoomOut();
		}
		
		scope.update();
	}
	
	function onMouseMove(event){
		if(!scope.enabled)return;
		//console.log('moving');
		event.preventDefault();
		
		var element = scope.canvas;
		
		if(state === STATES.ROTATE){
			if(scope.noRotate)return;
			//console.log('rotating');
			rotateEnd = event.clientY;
			rotateDelta = rotateEnd - rotateStart;
			
			scope.rotate(rotateDelta * scope.rotateSpeed);
			
			rotateStart = rotateEnd;
		}
		else if(state === STATES.PAN){
			if(scope.noPan)return;
			panEnd.set(event.clientX, event.clientY);
			panDelta.subVectors(panEnd, panStart);
			
			scope.pan(panDelta.x, panDelta.y);
			
			panStart.copy(panEnd);
		}
		scope.update();
	}
	
	function onMouseUp(event){
		if(!scope.enabled)return;
		
		document.removeEventListener('mousemove', onMouseMove, false);
		document.removeEventListener('mouseup', onMouseUp, false);
		state = STATES.NONE;
	}
	
	
	//event listeners
	this.canvas.addEventListener('contextmenu', function(event){event.preventDefault();}, false);
	this.canvas.addEventListener('mousedown', onMouseDown, false);
	this.canvas.addEventListener('mousewheel', onMouseWheel, false);
	this.canvas.addEventListener('DOMMouseScroll', onMouseWheel, false);	//firefox
	
	this.update();
};

GRAV.CameraControls.prototype = {
	constructor: GRAV.CameraControls,
	camera: undefined,
	canvas: undefined
	
	
};
/**
*	HistoryManager.js
*	5/31/2015
*
*	The arrays are laid out as follows: (trail length of 2)
*
*	history (two coordinates for each history position for each planet):
*	| plnt1.slot1.x | plnt1.slot1.y | plnt1.slot2.x | plnt1.slot2.y | plnt2.slot1.x | plnt2.slot1.y | ...
*
*	blockStart (one slot for each planet, indexes into this.history)
*	| planet1 | planet2 | planet3 | ...
*
*	tailStart (one slot for each planet, indexes into a history for one planet)
*	| planet1 | planet2 | planet3 | ...
*
*	transformedPts (two coordinates for each history position for each planet):
*	| plnt1.slot1.x | plnt1.slot1.y | plnt1.slot2.x | plnt1.slot2.y | plnt2.slot1.x | plnt2.slot1.y | ...
*
*	ptsNeedUpdate (as many slots as there are planets, indexes into history):
*	| idx1 | idx2 | idx3 | ...
*/

GRAV.HistoryManager = function(_params){
	_params = _params || {};
	
	//history length
	this.historyLength = Math.min(_params.historyLength, 256) || 100;
	//how many steps to skip between data points
	this.skip = _params.skip || 1;
	//engine capacity
	this.capacity = _params.capacity || 1024;
	
	this.dimensions = _params.dimensions || 2;
	
	this.init();
	
	this.clear();
};

GRAV.HistoryManager.prototype = {
	constructor: GRAV.HistoryManager,
	historyLength: 50,
	skip: 5,
	capacity: 1024,
	next: 0,	//the next available slot for storing a history
	last: -1,
	frame: 0,
	needsUpdateLatest: true,
	needsUpdateAll: true,
	history: undefined,
	blockStart: undefined,
	tailStart: undefined,
	transformedPts: undefined,
	ptsNeedUpdate: undefined,
	blockOwner: undefined,
	addPlanet: function(_x, _y, _idx){
		//find the next history slot
		var bs = this.blockStart,
		hist = this.history;
		var n = this.next, cap = this.capacity;
		while(this.blockOwner[n % cap] > -1){
			n++;
		}
		if(this.dimensions === 2){
			bs[_idx] = n * 2 * this.historyLength;
			this.tailStart[_idx] = 0;
			this.blockOwner[_idx] = _idx;
			for(var i = 0, len = this.historyLength, idx = bs[_idx], idy = idx + 1; i < len; i++, idx += 2, idy += 2){
				hist[idx] = _x;
				hist[idy] = _y;
			}
			this.ptsNeedUpdate[_idx] = -1;
		}
		this.last = n;
		this.next = n + 1;
		this.needsUpdateLatest = true;
		this.needsUpdateAll = true;
	},
	removePlanet: function(_idx){
		this.blockStart[_idx] = -1;
		this.tailStart[_idx] = 0;	//this one is optional
		this.blockOwner[_idx] = -1;
		this.ptsNeedUpdate[_idx] = -1;
	},
	needsUpdate: function(_frame){
		return _frame % this.skip === 0;
	},
	clear: function(){
		var bs = this.blockStart,
		pnu = this.ptsNeedUpdate,
		bo = this.blockOwner;
		
		//clear out the blockStart array
		//clear out the ptsNeedUpdate array
		for(var i = 0, len = this.capacity; i < len; i++){
			bs[i] = -1;
			pnu[i] = -1;
			bo[i] = -1;
		}
		this.last = -1;
		this.next = 0;
		this.needsUpdateLatest = true;
	},
	init: function(){
		//history
		this.history = new Float32Array(this.capacity * this.historyLength * this.dimensions);	//S_2*H_data
		//pointers into history array. This will need to be defragmented
		this.blockStart = new Int32Array(this.capacity);		//D_1_ID2*H
		this.tailStart = new Int16Array(this.capacity);			//D_1_ID2*H
		
		//transformed points
		this.transformedPts = new Float32Array(this.capacity * this.historyLength * this.dimensions);
		//which planets need to be updated and which point in the history needs the update
		//an index into this array corresponds to the same index for the planet in the Mass array
		//the values of this array refer to the index of the x-value of the position within the this.history array
		this.ptsNeedUpdate = new Int32Array(this.capacity);
		//who owns each history block
		this.blockOwner = new Int16Array(this.capacity);
		this.needsUpdateAll = true;
	},
	fill: function(_positions, _num){
		var posHist = this.history,
		bs = this.blockStart,
		ts = this.tailStart,
		pnu = this.ptsNeedUpdate
		bo = this.blockOwner,
		histLen = this.historyLength;
		if(this.dimensions === 2){
			for(var i = 0, idx = i * 2, idy = idx + 1; i < _num; i++, idx += 2, idy += 2){
				bs[i] = i * histLen * 2;
				ts[i] = 0;
				bo[i] = i;
				//fill in the history with the initial position
				for(var i2 = 0, idx2 = bs[i], idy2 = idx2 + 1; i2 < histLen; i2++, idx2 += 2, idy2 += 2){
					posHist[idx2] = _positions[idx];
					posHist[idy2] = _positions[idy];
				}
				//clear this entry out
				pnu[i] = -1;
			}
		}
		else if(this.dimension === 3){
			for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1; i < _num; i++, idx += 3, idy += 3, idz += 3){
				bs[i] = i * histLen * 3;
				ts[i] = 0;
				bo[i] = i;
				//fill in the history with the initial position
				for(var i2 = 0, idx2 = bs[i], idy2 = idx2 + 1, idz2 = idy2 + 1; i2 < histLen; i2++, idx2 += 3, idy2 += 3, idz2 += 3){
					posHist[idx2] = _positions[idx];
					posHist[idy2] = _positions[idy];
					posHist[idz2] = _positions[idz];
				}
				//clear this entry out
				pnu[i] = -1;
			}
		}
		this.next = _num;
		this.last = this.next - 1;
		this.needsUpdateLatest = true;
		this.needsUpdateAll = true;
	},
	update: function(){
		
	},
	resize: function(_length, _skip){
		this.skip = Math.max(_skip, 1);
		this.historyLength = _length;
		
		
		//TODO: preserve the old data?
		//we'd need to mess with blockOwner and blockStart if we were to resize after there's already data in the simulation
		this.init();
	},
	findNextBlock: function(){
		//find the next available block
		for(var i = 0, len = this.capacity; i < len; i++){
			//TODO
		}
	}
};





















/**
*	Renderer.js
*	5/21/2015
*
*	The renderer is responsible for rendering:
*	Planets, trails, off-screen planets
*/

GRAV.Renderer = function(){
	
};

GRAV.Renderer.prototype = {
	constructor: GRAV.Renderer,
	init: function(){
		//TODO?
	},
	render: function(){
		console.warn('GRAV.Renderer.render: Not implemented in base class.');
	},
	setSize: function(){
		console.warn('GRAV.Renderer.setSize: Not implemented in base class.');
	},
	setMaxCapacity: function(){
		console.warn('GRAV.Renderer.setMaxCapacity: Not implemented in base class.');
	},
	showDebugInfo: function(_show){
		this.showDebug = _show;
	},
	type: 'Renderer',
	viewSettings: {},
	canvas: undefined,
	ctx: undefined,
	width: 512,
	height: 512,
	showDebug: false,
	maxCapacity: 1024
};
/**
*	CanvasRenderer.js
*	5/21/2015
*/

GRAV.CanvasRenderer = function(_params){
	GRAV.Renderer.call(this);

	this.type = 'CanvasRenderer';
	
	_params = _params || {};
	
	//TODO: what if they don't give you a canvas?
	this.canvas = _params.canvas;
	this.ctx = this.canvas.getContext('2d');
	this.width = this.canvas.width;
	this.height = this.canvas.height;
	
	var vs = this.viewSettings;
	
	vs.bgColor = _params.bgColor || 0x000000;
	vs.planetColor = _params.planetColor || 0xDDDDDD;
	vs.showTrails = _params.showTrails || true;
	vs.trailLength = Math.min(_params.trailLength, 500) || 10;
	
	/*this.posHistory = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_trans = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_startIdx;*/
	this.pathLength = 100;
	this.tmpPath = new Float32Array(this.pathLength * 2);
};

GRAV.CanvasRenderer.prototype = Object.create(GRAV.Renderer.prototype);
GRAV.CanvasRenderer.prototype.constructor = GRAV.CanvasRenderer;

GRAV.CanvasRenderer.prototype.render = function(_state, _camera, _launcher, _history){
	var c = this.ctx;
	var vs = this.viewSettings;
	//_state is an object with two arrays: positions and radii
	//_state will also contain the number of valid planets in the arrays
	//_state also might contain an array of velocities
	//_camera is just a position, rotation, and zoom factor
	var visibility = _state.visibility,
	pos = _state.positions, rad = _state.radii;
	//have the camera transform all the positions
	_camera.applyMatrixToState(_state);
		
	//perform the camera's visibility check
	//var numVisible = _camera.checkVisibility(pos, rad, visibility, _state.numPlanets);
	var numVisible = _camera.checkVisibility(_state);
	var numInvisible = _state.numPlanets - numVisible;
	var numPlanets = _state.numPlanets;
	
	//clear the canvas
	//c.clearRect(0, 0, this.width, this.height);
	c.fillStyle = '#000000';
	c.fillRect(0,0,this.width,this.height);
	//draw the trails
	if(_history !== undefined && vs.showTrails){
		//apply the camera matrix to the trails
		_camera.applyMatrixToHistory(_history);
		var histLen = _history.historyLength,
		histLen2 = histLen * 2,
		transPts = _history.transformedPts,
		bs = _history.blockStart,
		ts = _history.tailStart
		posHist = _history.history;
		
		c.strokeStyle = '#555555';
		c.beginPath();
		
		//for each planet
		var blkstart = 0, tailstart = 0, curTail = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1, len = _state.size, count = 0; i < len && count < numPlanets; i++, idx += 2, idy += 2){
			//get the block start
			blkstart = bs[i];
			//get the tail start
			tailstart = ts[i];
			curTail = tailstart;
			//start at the planet's current position
			c.moveTo(pos[idx], pos[idy]);
			//move to the first
			//for each point in the trail
			for(var i2 = 0; i2 < histLen; i2++){
				//draw a line to the next point
				c.lineTo(transPts[blkstart + curTail], transPts[blkstart + curTail + 1]);
				//this advances forward through the loop while the engine goes backwards
				curTail = ((((curTail + 2) % histLen2) + histLen2) % histLen2);
			}
			count++;
		}
		c.stroke();
		//c.strokeStyle = '#550000';
	}
	//draw the planets
	c.fillStyle = '#888888';
	for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = _state.size; i < len && count < numVisible; i++, idx += 2, idy += 2){
		//skip invalid and off-screen ones
		if(visibility[i] !== 0)continue;	//0b0000
		count++;
		//draw a circle
		c.beginPath();
		c.arc(pos[idx], pos[idy], rad[i], 0, TWOPI);
		//c.closePath();
		c.fill();
	}
	//TODO: draw the planet directions? this would require passing in the velocities
	
	//draw the almost launched planet's trajectory
	//draw the almost launched planet, if any
	var tp = _launcher.tempPlanet;
	if(tp.enabled){
		var mat = _camera.matrixWorld;
		var e = mat.elements;
		
		var start = tp.tStrt,
		pos = tp.tPos;
		//transform it to screen space
		start.copy(tp.start);
		start.applyMatrix3(mat, 1);
		
		var path = this.tmpPath;
		_launcher.engine.getTrajectory(tp.position.x, tp.position.y, tp.velocity.x, tp.velocity.y, tp.mass, path, this.pathLength);
		c.beginPath();
		c.moveTo(pos.x, pos.y);
		var tx = 0, ty = 0;
		//transform the path into screen space		
		for(var i = 0, idx = i * 2, idy = idx + 1, len = this.pathLength; i < len; i++, idx += 2, idy += 2){
			tx = e[0] * path[idx] + e[3] * path[idy] + e[6];
			ty = e[1] * path[idx] + e[4] * path[idy] + e[7];
			c.lineTo(tx, ty);
		}
		c.strokeStyle = '#DDDD00';
		c.stroke();
		
		pos.copy(tp.position);
		pos.applyMatrix3(mat, 1);
		
		c.strokeStyle = '#777777';
		//draw a line between the two
		c.beginPath();
		c.moveTo(start.x, start.y);
		c.lineTo(pos.x, pos.y);
		c.stroke();
		
		//draw the planet
		c.beginPath();
		c.arc(pos.x, pos.y, tp.radius * _camera.scale.x, 0, TWOPI);
		c.fill();
	}
	
	//draw the off-screen planets
	var w = this.width, h = this.height;
	c.fillStyle = '#EA4B00';
	for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = _state.size; i < len && count < numInvisible; i++, idx += 2, idy += 2){
		count++;
		switch(visibility[i]){
			case 10: //0b1010 top-left
				c.beginPath();
				c.arc(5, 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 2: //0b0010 top-middle
				c.beginPath();
				c.arc(pos[idx], 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 6: //0b0110 top-right
				c.beginPath();
				c.arc(w - 5, 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 8: //0b1000 middle-left
				c.beginPath();
				c.arc(5, pos[idy], 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 4: //0b0100 middle-right
				c.beginPath();
				c.arc(w - 5, pos[idy], 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 9: //0b1001 bottom-left
				c.beginPath();
				c.arc(5, h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 1: //0b0001 bottom-middle
				c.beginPath();
				c.arc(pos[idx], h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 5: //0b0101 bottom-right
				c.beginPath();
				c.arc(w - 5, h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			default:	//either visible or invalid
				count--;
				break;
		}
	}
	
	//draw any on-screen information
		//number of planets
		//cursor if they're launching a planet?
	
	
	//draw any debug information
	if(this.showDebug){
		//draw cross hairs at the origin
		var mat = _camera.matrixWorld.elements;
		c.fillStyle = '#00DD00';
		c.beginPath();
		c.arc(mat[6], mat[7], 2.5, 0, TWOPI);
		c.fill();
		
		c.fillStyle = '#0000DD';
		c.beginPath();
		c.arc(_camera.width / 2, _camera.height/2, 2.5, 0, TWOPI);
		c.fill();
	}
	/*c.strokeStyle = '#555555';
	c.beginPath();
	c.moveTo(10,10);
	c.lineTo(100,100);
	c.moveTo(100,10);
	c.lineTo(190,100);
	c.closePath();
	c.stroke();*/
};

GRAV.CanvasRenderer.prototype.setMaxCapacity = function(_cap){
	this.maxCapacity = _cap;
	//TODO: resize the proper arrays
};

GRAV.CanvasRenderer.prototype.setSize = function(_w, _h){
	this.canvas.width = _w;
	this.canvas.height = _h;
	this.width = _w;
	this.height = _h;
};



/**
*	WebGLRenderer.js
*	6/8/2015
*/

GRAV.WebGLRenderer = function(_params){
	GRAV.Renderer.call(this);

	this.type = 'WebGLRenderer';
	
	_params = _params || {};
	
	//TODO: what if they don't give you a canvas?
	this.canvas = _params.canvas;
	this.ctx = this.canvas.getContext('webgl');
	this.width = this.canvas.width;
	this.height = this.canvas.height;
	
	var _alpha = _params.alpha !== undefined ? _params.alpha : false,
	_depth = _params.depth !== undefined ? _params.depth : true,
	_antialias = _params.antialias !== undefined ? _params.antialias : true,
	_clearColor = 0x000000;
	
	
	var vs = this.viewSettings;
	
	vs.bgColor = _params.bgColor || 0x000000;
	vs.planetColor = _params.planetColor || 0xDDDDDD;
	vs.showTrails = _params.showTrails || true;
	vs.trailLength = Math.min(_params.trailLength, 500) || 10;
	
	/*this.posHistory = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_trans = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_startIdx;*/
};

GRAV.WebGLRenderer.prototype = Object.create(GRAV.Renderer.prototype);
GRAV.WebGLRenderer.prototype.constructor = GRAV.WebGLRenderer;

GRAV.WebGLRenderer.prototype.render = function(_state, _camera, _history){
	
	var vs = this.viewSettings;
	//_state is an object with two arrays: positions and radii
	//_state will also contain the number of valid planets in the arrays
	//_state also might contain an array of velocities
	//_camera is just a position, rotation, and zoom factor
	var visibility = _state.visibility,
	pos = _state.positions, rad = _state.radii;
	//have the camera transform all the positions
	_camera.applyMatrixToState(_state);
		
	//perform the camera's visibility check
	//var numVisible = _camera.checkVisibility(pos, rad, visibility, _state.numPlanets);
	var numVisible = _camera.checkVisibility(_state);
	var numInvisible = _state.numPlanets - numVisible;
	var numPlanets = _state.numPlanets;
	
	
	//draw the trails
	if(_history !== undefined && vs.showTrails){
		//apply the camera matrix to the trails
		_camera.applyMatrixToHistory(_history);
		var histLen = _history.historyLength,
		histLen2 = histLen * 2,
		transPts = _history.transformedPts,
		bs = _history.blockStart,
		ts = _history.tailStart
		posHist = _history.history;
		
		
		
		
		
	}
	//TODO: draw the planets
	
	//TODO: draw the planet directions? this would require passing in the velocities
	
	//TODO: draw the almost launched planet, if any
	//TODO: draw the almost launched planet's trajectory
	
	//TODO: draw the off-screen planets
	
	
	//draw any on-screen information
		//number of planets
		//cursor if they're launching a planet?
	
	//draw any debug information
	if(this.showDebug){
		
	}
	
};

GRAV.WebGLRenderer.prototype.setMaxCapacity = function(_cap){
	this.maxCapacity = _cap;
	//TODO: resize the proper arrays
};

GRAV.WebGLRenderer.prototype.setSize = function(_w, _h){
	this.canvas.width = _w;
	this.canvas.height = _h;
	this.width = _w;
	this.height = _h;
};



/**
*	SimRunner.js
*	5/26/2015
*
*	This class is responsible for: maintaining a smooth framerate, playing, pausing, invoking the engine's
*	step function, invoking the Renderer's render function.
*/

GRAV.SimRunner = function(_params){
	_params = _params || {};
	
	this.engine = _params.engine;
	this.renderer = _params.renderer;
	this.camera = _params.camera;
	this.autoStart = (_params.autoStart === undefined)? false : _params.autoStart;
	this.launcher = _params.launcher;
	
	//get the initial state
	this.latestState = new GRAV.State(this.engine.getCapacity());
	var capacity = this.engine.getCapacity();
	this.engine.getState(this.latestState);
	if(this.autoStart){
		this.start();
	}
};

GRAV.SimRunner.prototype = {
	constructor: GRAV.SimRunner,
	engine: undefined,
	renderer: undefined,
	camera: undefined,
	paused: true,
	autoStart: true,
	started: false,
	latestState: undefined,
	start: function(){
		this.paused = false;
		this.keepStepping();
	},
	play: function(){
		this.paused = false;
		if(!this.started){
			this.start();
		}
	},
	keepStepping: function(){
		var ls = this.latestState,
		eng = this.engine;
		//redraw the frame
		this.renderer.render(ls, this.camera, this.launcher, eng.history);

		//wait for the next screen refresh
		requestAnimationFrame(this.keepStepping.bind(this));
		
		//call the engine's step function
		if(!this.paused){
			eng.step();
			eng.updateHistory();
		}
		//get the updated model state
		ls.numPlanets = eng.getState(ls);
		//ls.matrixApplied = false;
	},
	step: function(){
		this.engine.step();
		this.engine.updateHistory();
		this.redraw();
	},
	redraw: function(){
		var ls = this.latestState;
		this.engine.getState(ls);
		//ls.matrixApplied = false;
		this.renderer.render(ls, this.camera, this.launcher, this.engine.history);
	},
	pause: function(){
		this.paused = true;
	}
};

/**
*	Picker.js
*	6/4/2015
*	For selecting planets
*/

GRAV.Picker = function(_engine, _camera){
	this.engine = _engine;
	this.camera = _camera;
	
	this.size = 8;	//the 8 is arbitrary. I don't think it's possible to select more than 2 or 3 at once
	this.selected = new Int16Array(this.size);
	this.init();
};

GRAV.Picker.prototype = {
	constructor: GRAV.Picker,
	selected: undefined,
	camera: undefined,
	engine: undefined,
	pick: function(_x, _y){
		var e = this.camera.invMatrixWorld.elements;
		//clear out the selected array
		this.init();
		//convert the coordinates into model space
		var posx = e[0] * _x + e[3] * _y + e[6];
		var posy = e[1] * _x + e[4] * _y + e[7];
		//ask the engine if anyone's been selected
		var numSelected = this.engine.planetsAtPt(posx, posy, this.selected);
		return {
			num: numSelected,
			planets: Array.prototype.slice.call(this.selected)	//returns a normal array
		};
	},
	init: function(){
		for(var i = 0; i < this.size; i++){
			this.selected[i] = -1;
		}
	}
};/**
*	Launcher.js
*	6/8/2015
*/

GRAV.Launcher = function(_canvas, _cam, _engine){
	this.camera = _cam;
	this.canvas = _canvas;
	this.engine = _engine;
	
	this.enabled = false;
	
	var MOUSE = {LEFT: 0, MIDDLE: 1, RIGHT: 2};
	this.mouseButtons = { DRAG: MOUSE.LEFT };
	
	this.tempPlanet = {
		enabled: false,
		mass: 200,
		radius: Math.pow( ( 200 * PI3_4 ) , 0.3333),
		start: new GRAV.Vector2(),
		position: new GRAV.Vector2(),
		velocity: new GRAV.Vector2(),
		tStrt: new GRAV.Vector2(),
		tPos: new GRAV.Vector2()
	};
	
	//internals
	var scope = this;
	
	var dragStart = new GRAV.Vector2();
	var dragEnd = new GRAV.Vector2();
	var dragDelta = new GRAV.Vector2();
	
	var tmpMass = 200;
	
	var scrollStart = 0;
	var scrollEnd = 0;
	var scrollDelta = 0;
	
	var STATES = {NONE: -1, DRAG: 0, RESIZE: 1};
	var state = STATES.NONE;
	
	this.update = function(){
		var mat = this.camera.invMatrixWorld;
		var tmp = this.tempPlanet;
		tmp.enabled = true;
		
		tmp.mass = tmpMass;
		tmp.radius = Math.pow( ( tmpMass * PI3_4 ) , 0.3333);
		//transform the drag points into world space
		tmp.start.set(dragStart.x, dragStart.y);
		tmp.start.applyMatrix3(mat, 1);
		
		tmp.position.set(dragEnd.x, dragEnd.y);
		tmp.position.applyMatrix3(mat, 1);
		
		tmp.velocity.set(dragDelta.x, dragDelta.y);
		tmp.velocity.applyMatrix3(mat, 0);
		tmp.velocity.divideScalar(12);
	};
	
	function onMouseDown(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		var element = scope.canvas;
		var bbox = element.getBoundingClientRect();
		switch(event.button){
			case scope.mouseButtons.DRAG:
				state = STATES.DRAG;
				dragStart.set(event.clientX, event.clientY);
				break;
		}
		
		document.addEventListener('mousemove', onMouseMove, false);
		document.addEventListener('mouseup', onMouseUp, false);
	}
	
	function onMouseMove(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		var element = scope.canvas;
		
		if(state === STATES.DRAG){
			
			dragEnd.set(event.clientX, event.clientY);
			dragDelta.subVectors(dragStart, dragEnd);
			
			scope.update();
		}
	}
	
	function onMouseWheel(event){
		
	}
	
	function onMouseUp(event){
		if(!scope.enabled)return;
		
		//launch the planet
		if(state === STATES.DRAG){
			var t = scope.tempPlanet;
			
			scope.engine.addPlanet(t.position.x, t.position.y, 
									t.velocity.x, t.velocity.y,
									t.mass);
			
			t.enabled = false;
		}
		
		document.removeEventListener('mousemove', onMouseMove, false);
		document.removeEventListener('mouseup', onMouseUp, false);
		state = STATES.NONE;
	}
	
	this.canvas.addEventListener('contextmenu', function(event){event.preventDefault();}, false);
	this.canvas.addEventListener('mousedown', onMouseDown, false);
	this.canvas.addEventListener('mousewheel', onMouseWheel, false);
	this.canvas.addEventListener('DOMMouseScroll', onMouseWheel, false);	//firefox
};

GRAV.Launcher.prototype = {
	constructor: GRAV.Launcher,
	camera: undefined,
	canvas: undefined
};






















