::Navigate to the right folder. This %~dp0 business navigates to the .bat file's directory
pushd %~dp0
set THIS_DIR=%CD%
popd
cd %THIS_DIR%

::the old way:
::cd C:\Bitnami\wampstack-5.4.31-0\apache2\htdocs\grav_game_2\js

::Compile the whole thing together
set fpath=src\
set files=%fpath%Gravity.js %fpath%Math.js %fpath%Engine.js %fpath%State.js %fpath%Camera.js %fpath%CameraControls.js %fpath%HistoryManager.js %fpath%Renderer.js %fpath%CanvasRenderer.js %fpath%WebGLRenderer.js %fpath%SimRunner.js %fpath%Picker.js %fpath%Launcher.js

::Create a debug-friendly version
type %files% > grav.js

::Create a minified version
java -jar closure-compiler/compiler.jar --js_output_file=grav.min.js %files%
pause
