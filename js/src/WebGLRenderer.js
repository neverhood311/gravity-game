/**
*	WebGLRenderer.js
*	6/8/2015
*/

GRAV.WebGLRenderer = function(_params){
	GRAV.Renderer.call(this);

	this.type = 'WebGLRenderer';
	
	_params = _params || {};
	
	//TODO: what if they don't give you a canvas?
	this.canvas = _params.canvas;
	this.ctx = this.canvas.getContext('webgl');
	this.width = this.canvas.width;
	this.height = this.canvas.height;
	
	var _alpha = _params.alpha !== undefined ? _params.alpha : false,
	_depth = _params.depth !== undefined ? _params.depth : true,
	_antialias = _params.antialias !== undefined ? _params.antialias : true,
	_clearColor = 0x000000;
	
	
	var vs = this.viewSettings;
	
	vs.bgColor = _params.bgColor || 0x000000;
	vs.planetColor = _params.planetColor || 0xDDDDDD;
	vs.showTrails = _params.showTrails || true;
	vs.trailLength = Math.min(_params.trailLength, 500) || 10;
	
	/*this.posHistory = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_trans = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_startIdx;*/
};

GRAV.WebGLRenderer.prototype = Object.create(GRAV.Renderer.prototype);
GRAV.WebGLRenderer.prototype.constructor = GRAV.WebGLRenderer;

GRAV.WebGLRenderer.prototype.render = function(_state, _camera, _history){
	
	var vs = this.viewSettings;
	//_state is an object with two arrays: positions and radii
	//_state will also contain the number of valid planets in the arrays
	//_state also might contain an array of velocities
	//_camera is just a position, rotation, and zoom factor
	var visibility = _state.visibility,
	pos = _state.positions, rad = _state.radii;
	//have the camera transform all the positions
	_camera.applyMatrixToState(_state);
		
	//perform the camera's visibility check
	//var numVisible = _camera.checkVisibility(pos, rad, visibility, _state.numPlanets);
	var numVisible = _camera.checkVisibility(_state);
	var numInvisible = _state.numPlanets - numVisible;
	var numPlanets = _state.numPlanets;
	
	
	//draw the trails
	if(_history !== undefined && vs.showTrails){
		//apply the camera matrix to the trails
		_camera.applyMatrixToHistory(_history);
		var histLen = _history.historyLength,
		histLen2 = histLen * 2,
		transPts = _history.transformedPts,
		bs = _history.blockStart,
		ts = _history.tailStart
		posHist = _history.history;
		
		
		
		
		
	}
	//TODO: draw the planets
	
	//TODO: draw the planet directions? this would require passing in the velocities
	
	//TODO: draw the almost launched planet, if any
	//TODO: draw the almost launched planet's trajectory
	
	//TODO: draw the off-screen planets
	
	
	//draw any on-screen information
		//number of planets
		//cursor if they're launching a planet?
	
	//draw any debug information
	if(this.showDebug){
		
	}
	
};

GRAV.WebGLRenderer.prototype.setMaxCapacity = function(_cap){
	this.maxCapacity = _cap;
	//TODO: resize the proper arrays
};

GRAV.WebGLRenderer.prototype.setSize = function(_w, _h){
	this.canvas.width = _w;
	this.canvas.height = _h;
	this.width = _w;
	this.height = _h;
};



