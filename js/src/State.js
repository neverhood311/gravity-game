/**
*	State.js
*	5/28/2015
*	A class for transferring the current state of the simulation
*/

GRAV.State = function(_size){
	this.size = _size || 1024;
	this.positions = new Float32Array(this.size * 2);	//this doesn't have to be as accurate as the engine
	this.velocities = new Float32Array(this.size * 2);
	this.radii = new Float32Array(this.size);
	this.visibility = new Uint8Array(this.size);	//the value range is 0 to 15, so 8 bits is almost overkill
	this.created = new Int16Array(Math.ceil(this.size / 4));
	this.destroyed = new Int16Array(this.size);
};

GRAV.State.prototype = {
	constructor: GRAV.State,
	size: 1024,
	positions: undefined,
	velocities: undefined,
	radii: undefined,
	created: undefined,
	destroyed: undefined,
	numPlanets: 0,
	matrixApplied: false,
	setSize: function(_size){
		if(_size === undefined || _size === this.size){
			return;
		}
		this.size = _size;
		//TODO: resize each array
	},
	loadFromJSONstr: function(_jsonStr){
		//TODO
	},
	toJSONstr: function(){
		//TODO
	}
};

