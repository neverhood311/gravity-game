/**
*	CameraControls.js
*	5/27/2015
*/

GRAV.CameraControls = function(_cam, _canvas){
	this.camera = _cam;
	this.canvas = _canvas;
	
	this.enabled = true;
	
	this.position = new GRAV.Vector2();
	this.rotation = 0;	//stored in degrees
	
	this.zoom = 1.0;
	
	this.noZoom = false;
	this.zoomSpeed = 1.0;
	
	this.minZoom = 0.0005;
	this.maxZoom = 100;
	
	this.noRotate = false;
	this.rotateSpeed = 1.0;
	
	this.noPan = false;
	
	var MOUSE = {LEFT: 0, MIDDLE: 1, RIGHT: 2};
	this.mouseButtons = { PAN: MOUSE.LEFT, ROTATE: MOUSE.RIGHT };
	
	//internals
	var scope = this;
	
	this.position0 = this.position.clone();
	
	var panStart = new GRAV.Vector2();
	var panEnd = new GRAV.Vector2();
	var panDelta = new GRAV.Vector2();
	var panOffset = new GRAV.Vector2();
	
	var rotateStart = 0;
	var rotateEnd = 0;
	var rotateDelta = 0;
	var STATES = {NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2};
	var state = STATES.NONE;
	
	//functions
	this.pan = function(_deltaX, _deltaY){
		if(this.noPan)return;
		
		panOffset.set(_deltaX, _deltaY);
		//apply the zoom factor and rotation
		panOffset.x /= this.zoom;
		panOffset.y /= this.zoom;
		var c = Math.cos(this.rotation * PI_180),
		s = Math.sin(this.rotation * PI_180);
		var panx = panOffset.x * c + panOffset.y * s;
		var pany = panOffset.x * -s + panOffset.y * c;
		
		panOffset.set(panx, pany);
		
		//panOffset.applyMatrix3(this.camera.invMatrixWorld, 0);
		
		this.position.add(panOffset);
	};
	this.rotate = function(_num){
		if(this.noRotate)return;
		this.rotation += _num * 0.333;
	};
	this.zoomIn = function(){
		this.zoom *= 1.1 * this.zoomSpeed;
		this.zoom = Math.min(this.zoom, this.maxZoom);
	};
	this.zoomOut = function(){
		this.zoom *= 0.9 * this.zoomSpeed;
		this.zoom = Math.max(this.zoom, this.minZoom);
	};
	this.reset = function(){
		//state = STATES.NONE;
		this.position.copy(this.position0);
		this.rotation = 0;
		this.zoom = 1.0;
		this.update();
	};
	this.update = function(){
		//apply the position, rotation, and zoom to the camera
		this.camera.setPosition(this.position.x, this.position.y, false);
		this.camera.setScale(this.zoom, false);
		this.camera.setRotationDeg(this.rotation, false);
		this.camera.updateMatrix();
	};
	
	function onMouseDown(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		switch(event.button){
			case scope.mouseButtons.PAN:
				state = STATES.PAN;
				panStart.set(event.clientX, event.clientY);
				break;
			case scope.mouseButtons.ROTATE:
				state = STATES.ROTATE;
				rotateStart = event.clientY;
				break;
		}
		
		document.addEventListener('mousemove', onMouseMove, false);
		document.addEventListener('mouseup', onMouseUp, false);
	}
	
	function onMouseWheel(event){
		if(!scope.enabled || scope.noZoom)return;
		
		event.preventDefault();
		event.stopPropagation();
		
		var delta = 0;
		
		if(event.wheelDelta !== undefined){
			delta = event.wheelDelta;
		}
		else if(event.detail !== undefined){
			delta = -event.detail;
		}
		
		if(delta > 0){
			scope.zoomIn();
		}
		else{
			scope.zoomOut();
		}
		
		scope.update();
	}
	
	function onMouseMove(event){
		if(!scope.enabled)return;
		//console.log('moving');
		event.preventDefault();
		
		var element = scope.canvas;
		
		if(state === STATES.ROTATE){
			if(scope.noRotate)return;
			//console.log('rotating');
			rotateEnd = event.clientY;
			rotateDelta = rotateEnd - rotateStart;
			
			scope.rotate(rotateDelta * scope.rotateSpeed);
			
			rotateStart = rotateEnd;
		}
		else if(state === STATES.PAN){
			if(scope.noPan)return;
			panEnd.set(event.clientX, event.clientY);
			panDelta.subVectors(panEnd, panStart);
			
			scope.pan(panDelta.x, panDelta.y);
			
			panStart.copy(panEnd);
		}
		scope.update();
	}
	
	function onMouseUp(event){
		if(!scope.enabled)return;
		
		document.removeEventListener('mousemove', onMouseMove, false);
		document.removeEventListener('mouseup', onMouseUp, false);
		state = STATES.NONE;
	}
	
	
	//event listeners
	this.canvas.addEventListener('contextmenu', function(event){event.preventDefault();}, false);
	this.canvas.addEventListener('mousedown', onMouseDown, false);
	this.canvas.addEventListener('mousewheel', onMouseWheel, false);
	this.canvas.addEventListener('DOMMouseScroll', onMouseWheel, false);	//firefox
	
	this.update();
};

GRAV.CameraControls.prototype = {
	constructor: GRAV.CameraControls,
	camera: undefined,
	canvas: undefined
	
	
};
