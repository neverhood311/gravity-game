/**
*	Renderer.js
*	5/21/2015
*
*	The renderer is responsible for rendering:
*	Planets, trails, off-screen planets
*/

GRAV.Renderer = function(){
	
};

GRAV.Renderer.prototype = {
	constructor: GRAV.Renderer,
	init: function(){
		//TODO?
	},
	render: function(){
		console.warn('GRAV.Renderer.render: Not implemented in base class.');
	},
	setSize: function(){
		console.warn('GRAV.Renderer.setSize: Not implemented in base class.');
	},
	setMaxCapacity: function(){
		console.warn('GRAV.Renderer.setMaxCapacity: Not implemented in base class.');
	},
	showDebugInfo: function(_show){
		this.showDebug = _show;
	},
	type: 'Renderer',
	viewSettings: {},
	canvas: undefined,
	ctx: undefined,
	width: 512,
	height: 512,
	showDebug: false,
	maxCapacity: 1024
};
