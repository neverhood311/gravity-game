/**
*	Math.js
*	5/21/2015
*	Borrowed heavily from mrdoob/three.js
*/

var PI = 3.141592653589793;
var PI_180 = PI / 180;
var PI4 = 4 * PI;
var PI3_4 = 3 / PI4;
var TWOPI = PI * 2;

GRAV.Matrix3 = function(){
	this.elements = new Float32Array([
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	]);
};

GRAV.Matrix3.prototype = {
	constructor: GRAV.Matrix3,
	elements: undefined,
	set: function(_n11, _n12, _n13, _n21, _n22, _n23, _n31, _n32, _n33){
		var te = this.elements;
		te[ 0 ] = _n11; te[ 3 ] = _n12; te[ 6 ] = _n13;
		te[ 1 ] = _n21; te[ 4 ] = _n22; te[ 7 ] = _n23;
		te[ 2 ] = _n31; te[ 5 ] = _n32; te[ 8 ] = _n33;
		return this;
	},
	identity: function(){
		this.set(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		);
		return this;
	},
	rotate: function(_rad){
		var s = Math.sin(_rad),
		c = Math.cos(_rad);
		var e = this.elements;
		
		var e0 = e[0] * c + e[3] * s,
		e1 = e[1] * c + e[4] * s,

		e3 = e[0] * -s + e[3] * c,
		e4 = e[1] * -s + e[4] * c;
		
		e[0] = e0;
		e[1] = e1;
		e[3] = e3;
		e[4] = e4;
		
		return this;
	},
	makeRotation: function(_rad){
		var s_rad = Math.sin(_rad),
		c_rad = Math.cos(_rad);
		this.set(
			c_rad, -s_rad, 0,
			s_rad, c_rad, 0,
			0, 0, 1
		);
		return this;
	},
	translate: function(_trans){
		var e = this.elements;
		var x = _trans.x,
		y = _trans.y;
		
		//e[0] = e[0] * 1
		//e[4] = e[4] * 1
		
		e[6] = e[0] * x + e[3] * y + e[6];
		e[7] = e[1] * x + e[4] * y + e[7];
		e[8] = e[2] * x + e[5] * y + e[8];
		
		return this;
	},
	setPosition: function(_pos){
		var e = this.elements;
		e[6] = _pos.x;
		e[7] = _pos.y;
		return this;
	},
	makeTranslation: function(_pos){
		this.set(
			1, 0, _pos.x,
			0, 1, _pos.y,
			0, 0, 1
		);
		return this;		
	},
	scale: function(_sc){
		var te = this.elements;
		var x = _sc.x, y = _sc.y;
		te[0] *= x; te[3] *= y;
		te[1] *= x; te[4] *= y;
		return this;
	},
	makeScale: function(_sc){
		this.set(
			_sc.x, 0, 0,
			0, _sc.y, 0,
			0, 0, 1
		);
		return this;
	},
	multiply: function(_m){
		var me = _m.elements,
		te = this.elements;
		
		var t0 = te[0] * me[0] + te[3] * me[1] + te[6] * me[2],
			t1 = te[1] * me[0] + te[4] * me[1] + te[7] * me[2],
			t2 = te[2] * me[0] + te[5] * me[1] + te[8] * me[2],
			
			t3 = te[0] * me[3] + te[3] * me[4] + te[6] * me[5],
			t4 = te[1] * me[3] + te[4] * me[4] + te[7] * me[5],
			t5 = te[2] * me[3] + te[5] * me[4] + te[8] * me[5],
			
			t6 = te[0] * me[6] + te[3] * me[7] + te[6] * me[8],
			t7 = te[1] * me[6] + te[4] * me[7] + te[7] * me[8],
			t8 = te[2] * me[6] + te[5] * me[7] + te[8] * me[8];
		
		te[0] = t0;
		te[1] = t1;
		te[2] = t2;
		te[3] = t3;
		te[4] = t4;
		te[5] = t5;
		te[6] = t6;
		te[7] = t7;
		te[8] = t8;
		
		return this;
	},
	getInverse: function(_matrix){
		
		var te = this.elements, me = _matrix.elements;
		//ad - cb
		te[0] = me[4] * me[8] - me[5] * me[7];
		te[1] = me[7] * me[2] - me[8] * me[1];
		te[2] = me[1] * me[5] - me[2] * me[4];
		te[3] = me[6] * me[5] - me[8] * me[3];
		te[4] = me[0] * me[8] - me[2] * me[6];
		te[5] = me[3] * me[2] - me[0] * me[5];
		te[6] = me[3] * me[7] - me[4] * me[6];
		te[7] = me[6] * me[1] - me[7] * me[0];
		te[8] = me[0] * me[4] - me[1] * me[3];
		
		var det = _matrix.determinant();
		if(det === 0){
			console.warn('GRAV.Matrix3.getInverse: can\'t invert matrix; determinant is 0');
			return;
		}
		det = 1 / det;
		for(var i = 0; i < 9; i++){
			te[i] *= det;
		}
		
		return this;
	},
	determinant: function(){
		var te = this.elements;

		var a = te[ 0 ], b = te[ 1 ], c = te[ 2 ],
			d = te[ 3 ], e = te[ 4 ], f = te[ 5 ],
			g = te[ 6 ], h = te[ 7 ], i = te[ 8 ];

		return a * e * i - a * f * h - b * d * i + b * f * g + c * d * h - c * e * g;
	},
	copy: function(_m){
		var me = _m.elements;
		this.set(
			me[ 0 ], me[ 3 ], me[ 6 ],
			me[ 1 ], me[ 4 ], me[ 7 ],
			me[ 2 ], me[ 5 ], me[ 8 ]
		);
		return this;
	},
	fromArray: function(_array){
		this.elements.set(_array);
		return this;
	},
	toArray: function(){
		var te = this.elements;
		return [
			te[ 0 ], te[ 1 ], te[ 2 ],
			te[ 3 ], te[ 4 ], te[ 5 ],
			te[ 6 ], te[ 7 ], te[ 8 ]
		];
	},
	equals: function(_m){
		var me = _m.elements, te = this.elements;
		for(var i = 0; i < 9; i++){
			if(me[i] !== te[i])return false;
		}
		return true;
	},
	clone: function(){
		return new GRAV.Matrix3().fromArray(this.elements);
	}
};


GRAV.Vector2 = function(_x, _y){
	this.x = _x || 0;
	this.y = _y || 0;
};

GRAV.Vector2.prototype = {
	constructor: GRAV.Vector2,
	x: 0,
	y: 0,
	set: function(_x, _y){
		this.x = _x;
		this.y = _y;
	},
	copy: function(_v){
		this.x = _v.x;
		this.y = _v.y;
		return this;
	},
	add: function(_v){
		this.x += _v.x;
		this.y += _v.y;
		return this;
	},
	addVectors: function(_a, _b){
		this.x = _a.x + _b.x;
		this.y = _a.y + _b.y;
		return this;
	},
	sub: function(_v){
		this.x -= _v.x;
		this.y -= _v.y;
		return this;
	},
	subVectors: function(_a, _b){
		this.x = _a.x - _b.x;
		this.y = _a.y - _b.y;
		return this;
	},
	multiply: function(_v){
		this.x *= _v.x;
		this.y *= _v.y;
		return this;
	},
	divideScalar: function(_s){
		this.x /= _s;
		this.y /= _s;
		return this;
	},
	min: function(_v){
		if (this.x > _v.x){
			this.x = _v.x;
		}

		if (this.y > _v.y){
			this.y = _v.y;
		}
		return this;
	},
	max: function(_v){
		if (this.x < _v.x){
			this.x = _v.x;
		}

		if (this.y < _v.y){
			this.y = _v.y;
		}
		return this;
	},
	//_isPoint is either 1 or 0, NOT a boolean
	applyMatrix3: function(_m, _isPoint){
		var x = this.x;
		var y = this.y;
		var z = _isPoint === undefined? 1 : _isPoint;

		var e = _m.elements;

		this.x = e[ 0 ] * x + e[ 3 ] * y + e[ 6 ] * z;
		this.y = e[ 1 ] * x + e[ 4 ] * y + e[ 7 ] * z;
		//this.z = e[ 2 ] * x + e[ 5 ] * y + e[ 8 ] * z;
		return this;
	},
	dot: function(_v){
		return this.x * _v.x + this.y * _v.y;
	},
	lengthSq: function(){
		return this.x * this.x + this.y * this.y;
	},
	length: function(){
		return Math.sqrt(this.x * this.x + this.y * this.y);
	},
	normalize: function(){
		return this.divideScalar(this.length());
	},
	equals: function(_v){
		return ((_v.x === this.x) && (_v.y === this.y));
	},
	fromArray: function(_array, _offset){
		if(_offset === undefined)_offset = 0;

		this.x = _array[_offset];
		this.y = _array[_offset + 1];

		return this;
	},
	toArray: function(_array, _offset){
		if (_array === undefined)_array = [];
		if (_offset === undefined)_offset = 0;

		array[_offset] = this.x;
		array[_offset + 1] = this.y;

		return _array;
	},
	clone: function(){
		return new GRAV.Vector2(this.x, this.y);
	}
};








