/**
*	CanvasRenderer.js
*	5/21/2015
*/

GRAV.CanvasRenderer = function(_params){
	GRAV.Renderer.call(this);

	this.type = 'CanvasRenderer';
	
	_params = _params || {};
	
	//TODO: what if they don't give you a canvas?
	this.canvas = _params.canvas;
	this.ctx = this.canvas.getContext('2d');
	this.width = this.canvas.width;
	this.height = this.canvas.height;
	
	var vs = this.viewSettings;
	
	vs.bgColor = _params.bgColor || 0x000000;
	vs.planetColor = _params.planetColor || 0xDDDDDD;
	vs.showTrails = _params.showTrails || true;
	vs.trailLength = Math.min(_params.trailLength, 500) || 10;
	
	/*this.posHistory = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_trans = new Float32Array(this.maxCapacity * vs.trailLength * 2);
	this.posHistory_startIdx;*/
	this.pathLength = 100;
	this.tmpPath = new Float32Array(this.pathLength * 2);
};

GRAV.CanvasRenderer.prototype = Object.create(GRAV.Renderer.prototype);
GRAV.CanvasRenderer.prototype.constructor = GRAV.CanvasRenderer;

GRAV.CanvasRenderer.prototype.render = function(_state, _camera, _launcher, _history){
	var c = this.ctx;
	var vs = this.viewSettings;
	//_state is an object with two arrays: positions and radii
	//_state will also contain the number of valid planets in the arrays
	//_state also might contain an array of velocities
	//_camera is just a position, rotation, and zoom factor
	var visibility = _state.visibility,
	pos = _state.positions, rad = _state.radii;
	//have the camera transform all the positions
	_camera.applyMatrixToState(_state);
		
	//perform the camera's visibility check
	//var numVisible = _camera.checkVisibility(pos, rad, visibility, _state.numPlanets);
	var numVisible = _camera.checkVisibility(_state);
	var numInvisible = _state.numPlanets - numVisible;
	var numPlanets = _state.numPlanets;
	
	//clear the canvas
	//c.clearRect(0, 0, this.width, this.height);
	c.fillStyle = '#000000';
	c.fillRect(0,0,this.width,this.height);
	//draw the trails
	if(_history !== undefined && vs.showTrails){
		//apply the camera matrix to the trails
		_camera.applyMatrixToHistory(_history);
		var histLen = _history.historyLength,
		histLen2 = histLen * 2,
		transPts = _history.transformedPts,
		bs = _history.blockStart,
		ts = _history.tailStart
		posHist = _history.history;
		
		c.strokeStyle = '#555555';
		c.beginPath();
		
		//for each planet
		var blkstart = 0, tailstart = 0, curTail = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1, len = _state.size, count = 0; i < len && count < numPlanets; i++, idx += 2, idy += 2){
			//get the block start
			blkstart = bs[i];
			//get the tail start
			tailstart = ts[i];
			curTail = tailstart;
			//start at the planet's current position
			c.moveTo(pos[idx], pos[idy]);
			//move to the first
			//for each point in the trail
			for(var i2 = 0; i2 < histLen; i2++){
				//draw a line to the next point
				c.lineTo(transPts[blkstart + curTail], transPts[blkstart + curTail + 1]);
				//this advances forward through the loop while the engine goes backwards
				curTail = ((((curTail + 2) % histLen2) + histLen2) % histLen2);
			}
			count++;
		}
		c.stroke();
		//c.strokeStyle = '#550000';
	}
	//draw the planets
	c.fillStyle = '#888888';
	for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = _state.size; i < len && count < numVisible; i++, idx += 2, idy += 2){
		//skip invalid and off-screen ones
		if(visibility[i] !== 0)continue;	//0b0000
		count++;
		//draw a circle
		c.beginPath();
		c.arc(pos[idx], pos[idy], rad[i], 0, TWOPI);
		//c.closePath();
		c.fill();
	}
	//TODO: draw the planet directions? this would require passing in the velocities
	
	//draw the almost launched planet's trajectory
	//draw the almost launched planet, if any
	var tp = _launcher.tempPlanet;
	if(tp.enabled){
		var mat = _camera.matrixWorld;
		var e = mat.elements;
		
		var start = tp.tStrt,
		pos = tp.tPos;
		//transform it to screen space
		start.copy(tp.start);
		start.applyMatrix3(mat, 1);
		
		var path = this.tmpPath;
		_launcher.engine.getTrajectory(tp.position.x, tp.position.y, tp.velocity.x, tp.velocity.y, tp.mass, path, this.pathLength);
		c.beginPath();
		c.moveTo(pos.x, pos.y);
		var tx = 0, ty = 0;
		//transform the path into screen space		
		for(var i = 0, idx = i * 2, idy = idx + 1, len = this.pathLength; i < len; i++, idx += 2, idy += 2){
			tx = e[0] * path[idx] + e[3] * path[idy] + e[6];
			ty = e[1] * path[idx] + e[4] * path[idy] + e[7];
			c.lineTo(tx, ty);
		}
		c.strokeStyle = '#DDDD00';
		c.stroke();
		
		pos.copy(tp.position);
		pos.applyMatrix3(mat, 1);
		
		c.strokeStyle = '#777777';
		//draw a line between the two
		c.beginPath();
		c.moveTo(start.x, start.y);
		c.lineTo(pos.x, pos.y);
		c.stroke();
		
		//draw the planet
		c.beginPath();
		c.arc(pos.x, pos.y, tp.radius * _camera.scale.x, 0, TWOPI);
		c.fill();
	}
	
	//draw the off-screen planets
	var w = this.width, h = this.height;
	c.fillStyle = '#EA4B00';
	for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = _state.size; i < len && count < numInvisible; i++, idx += 2, idy += 2){
		count++;
		switch(visibility[i]){
			case 10: //0b1010 top-left
				c.beginPath();
				c.arc(5, 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 2: //0b0010 top-middle
				c.beginPath();
				c.arc(pos[idx], 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 6: //0b0110 top-right
				c.beginPath();
				c.arc(w - 5, 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 8: //0b1000 middle-left
				c.beginPath();
				c.arc(5, pos[idy], 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 4: //0b0100 middle-right
				c.beginPath();
				c.arc(w - 5, pos[idy], 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 9: //0b1001 bottom-left
				c.beginPath();
				c.arc(5, h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 1: //0b0001 bottom-middle
				c.beginPath();
				c.arc(pos[idx], h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			case 5: //0b0101 bottom-right
				c.beginPath();
				c.arc(w - 5, h - 5, 2.5, 0, TWOPI);
				//c.closePath();
				c.fill();
				break;
			default:	//either visible or invalid
				count--;
				break;
		}
	}
	
	//draw any on-screen information
		//number of planets
		//cursor if they're launching a planet?
	
	
	//draw any debug information
	if(this.showDebug){
		//draw cross hairs at the origin
		var mat = _camera.matrixWorld.elements;
		c.fillStyle = '#00DD00';
		c.beginPath();
		c.arc(mat[6], mat[7], 2.5, 0, TWOPI);
		c.fill();
		
		c.fillStyle = '#0000DD';
		c.beginPath();
		c.arc(_camera.width / 2, _camera.height/2, 2.5, 0, TWOPI);
		c.fill();
	}
	/*c.strokeStyle = '#555555';
	c.beginPath();
	c.moveTo(10,10);
	c.lineTo(100,100);
	c.moveTo(100,10);
	c.lineTo(190,100);
	c.closePath();
	c.stroke();*/
};

GRAV.CanvasRenderer.prototype.setMaxCapacity = function(_cap){
	this.maxCapacity = _cap;
	//TODO: resize the proper arrays
};

GRAV.CanvasRenderer.prototype.setSize = function(_w, _h){
	this.canvas.width = _w;
	this.canvas.height = _h;
	this.width = _w;
	this.height = _h;
};



