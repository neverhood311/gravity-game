/**
*	Launcher.js
*	6/8/2015
*/

GRAV.Launcher = function(_canvas, _cam, _engine){
	this.camera = _cam;
	this.canvas = _canvas;
	this.engine = _engine;
	
	this.enabled = false;
	
	var MOUSE = {LEFT: 0, MIDDLE: 1, RIGHT: 2};
	this.mouseButtons = { DRAG: MOUSE.LEFT };
	
	this.tempPlanet = {
		enabled: false,
		mass: 200,
		radius: Math.pow( ( 200 * PI3_4 ) , 0.3333),
		start: new GRAV.Vector2(),
		position: new GRAV.Vector2(),
		velocity: new GRAV.Vector2(),
		tStrt: new GRAV.Vector2(),
		tPos: new GRAV.Vector2()
	};
	
	//internals
	var scope = this;
	
	var dragStart = new GRAV.Vector2();
	var dragEnd = new GRAV.Vector2();
	var dragDelta = new GRAV.Vector2();
	
	var tmpMass = 200;
	
	var scrollStart = 0;
	var scrollEnd = 0;
	var scrollDelta = 0;
	
	var STATES = {NONE: -1, DRAG: 0, RESIZE: 1};
	var state = STATES.NONE;
	
	this.update = function(){
		var mat = this.camera.invMatrixWorld;
		var tmp = this.tempPlanet;
		tmp.enabled = true;
		
		tmp.mass = tmpMass;
		tmp.radius = Math.pow( ( tmpMass * PI3_4 ) , 0.3333);
		//transform the drag points into world space
		tmp.start.set(dragStart.x, dragStart.y);
		tmp.start.applyMatrix3(mat, 1);
		
		tmp.position.set(dragEnd.x, dragEnd.y);
		tmp.position.applyMatrix3(mat, 1);
		
		tmp.velocity.set(dragDelta.x, dragDelta.y);
		tmp.velocity.applyMatrix3(mat, 0);
		tmp.velocity.divideScalar(12);
	};
	
	function onMouseDown(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		var element = scope.canvas;
		var bbox = element.getBoundingClientRect();
		switch(event.button){
			case scope.mouseButtons.DRAG:
				state = STATES.DRAG;
				dragStart.set(event.clientX, event.clientY);
				break;
		}
		
		document.addEventListener('mousemove', onMouseMove, false);
		document.addEventListener('mouseup', onMouseUp, false);
	}
	
	function onMouseMove(event){
		if(!scope.enabled)return;
		event.preventDefault();
		
		var element = scope.canvas;
		
		if(state === STATES.DRAG){
			
			dragEnd.set(event.clientX, event.clientY);
			dragDelta.subVectors(dragStart, dragEnd);
			
			scope.update();
		}
	}
	
	function onMouseWheel(event){
		
	}
	
	function onMouseUp(event){
		if(!scope.enabled)return;
		
		//launch the planet
		if(state === STATES.DRAG){
			var t = scope.tempPlanet;
			
			scope.engine.addPlanet(t.position.x, t.position.y, 
									t.velocity.x, t.velocity.y,
									t.mass);
			
			t.enabled = false;
		}
		
		document.removeEventListener('mousemove', onMouseMove, false);
		document.removeEventListener('mouseup', onMouseUp, false);
		state = STATES.NONE;
	}
	
	this.canvas.addEventListener('contextmenu', function(event){event.preventDefault();}, false);
	this.canvas.addEventListener('mousedown', onMouseDown, false);
	this.canvas.addEventListener('mousewheel', onMouseWheel, false);
	this.canvas.addEventListener('DOMMouseScroll', onMouseWheel, false);	//firefox
};

GRAV.Launcher.prototype = {
	constructor: GRAV.Launcher,
	camera: undefined,
	canvas: undefined
};






















