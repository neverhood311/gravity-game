/**
*	SimRunner.js
*	5/26/2015
*
*	This class is responsible for: maintaining a smooth framerate, playing, pausing, invoking the engine's
*	step function, invoking the Renderer's render function.
*/

GRAV.SimRunner = function(_params){
	_params = _params || {};
	
	this.engine = _params.engine;
	this.renderer = _params.renderer;
	this.camera = _params.camera;
	this.autoStart = (_params.autoStart === undefined)? false : _params.autoStart;
	this.launcher = _params.launcher;
	
	//get the initial state
	this.latestState = new GRAV.State(this.engine.getCapacity());
	var capacity = this.engine.getCapacity();
	this.engine.getState(this.latestState);
	if(this.autoStart){
		this.start();
	}
};

GRAV.SimRunner.prototype = {
	constructor: GRAV.SimRunner,
	engine: undefined,
	renderer: undefined,
	camera: undefined,
	paused: true,
	autoStart: true,
	started: false,
	latestState: undefined,
	start: function(){
		this.paused = false;
		this.keepStepping();
	},
	play: function(){
		this.paused = false;
		if(!this.started){
			this.start();
		}
	},
	keepStepping: function(){
		var ls = this.latestState,
		eng = this.engine;
		//redraw the frame
		this.renderer.render(ls, this.camera, this.launcher, eng.history);

		//wait for the next screen refresh
		requestAnimationFrame(this.keepStepping.bind(this));
		
		//call the engine's step function
		if(!this.paused){
			eng.step();
			eng.updateHistory();
		}
		//get the updated model state
		ls.numPlanets = eng.getState(ls);
		//ls.matrixApplied = false;
	},
	step: function(){
		this.engine.step();
		this.engine.updateHistory();
		this.redraw();
	},
	redraw: function(){
		var ls = this.latestState;
		this.engine.getState(ls);
		//ls.matrixApplied = false;
		this.renderer.render(ls, this.camera, this.launcher, this.engine.history);
	},
	pause: function(){
		this.paused = true;
	}
};

