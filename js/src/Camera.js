/**
*	Camera.js
*	5/21/2015
*/

GRAV.Camera = function(){
	this.position = new GRAV.Vector2();
	this.negPosition = new GRAV.Vector2();
	this.scale = new GRAV.Vector2(1,1);
	this.matrixWorld = new GRAV.Matrix3();
	this.invMatrixWorld = new GRAV.Matrix3();
	this.viewport = new GRAV.Vector2();
	this.setSize(512, 512);
};

GRAV.Camera.prototype = {
	constructor: GRAV.Camera,
	matrixWorld: undefined,
	invMatrixWorld: undefined,
	position: undefined,
	negPosition: undefined,
	scale: undefined,
	rotation: 0,	//in radians
	h_viewport: undefined,
	width: 0,
	height: 0,
	h_width: 0,
	h_height: 0,
	changed: true,
	setSize: function(_w, _h, _update){
		if(this.h_viewport === undefined){
			this.h_viewport = new GRAV.Vector2();
		}
		this.width = _w;
		this.h_width = _w * 0.5;
		this.height = _h;
		this.h_height = _h * 0.5;
		this.h_viewport.set(this.h_width, this.h_height);
		if(_update === false)return;
		this.updateMatrix();
	},
	setPosition: function(_x, _y, _update){
		this.position.set(_x, _y);
		this.negPosition.set(-_x, -_y);
		if(_update === false)return;
		this.updateMatrix();
	},
	setScale: function(_sc, _update){
		this.scale.set(_sc, _sc);
		if(_update === false)return;
		this.updateMatrix();
	},
	setScale2D: function(_x, _y, _update){
		this.scale.set(_x, _y);
		if(_update === false)return;
		this.updateMatrix();
	},
	setRotation: function(_rad, _update){
		this.rotation = _rad;
		if(_update === false)return;
		this.updateMatrix();
	},
	setRotationDeg: function(_deg, _update){
		this.setRotation(_deg * PI_180, _update);
	},
	updateMatrix: function(){
		//Translation
		//Scale
		//Rotation
		this.matrixWorld.identity().setPosition(this.h_viewport).translate(this.position).translate(this.negPosition).scale(this.scale).rotate(this.rotation).translate(this.position);
		
		this.invMatrixWorld.getInverse(this.matrixWorld);
		
		this.changed = true;
	},
	//pass in the radii so that we can skip any invalid planets
	applyMatrixToState: function(_state){
		if(_state.matrixApplied)return;
		
		var pos = _state.positions,
		radii = _state.radii,
		num = _state.numPlanets;
		var e = this.matrixWorld.elements,
		sc = this.scale.x,
		posx = 0, posy = 0,
		size = _state.size;
		for(var i = 0, idx = i * 2, idy = idx + 1, count = 0; count < num && i < size; i++, idx += 2, idy += 2){
			//if it's a real planet
			if(radii[i] > 0){
				//apply the matrix to the position
				posx = e[0] * pos[idx] + e[3] * pos[idy] + e[6];
				posy = e[1] * pos[idx] + e[4] * pos[idy] + e[7];
				pos[idx] = posx;
				pos[idy] = posy;
				
				//apply the scale to the radius
				radii[i] *= sc;
				
				count++;
			}
		}
		_state.matrixApplied = true;
	},
	applyMatrixToHistory: function(_history){
		var e = this.matrixWorld.elements,
		tpts = _history.transformedPts,
		posHist = _history.history,
		pnu = _history.ptsNeedUpdate;
		var posx = 0, posy = 0;
		if(this.changed || _history.needsUpdateAll){
			//update everything
			//only update the entries that need updating
			var start = -1,
			histLen = _history.historyLength,
			bs = _history.blockStart;
			for(var i = 0, len = _history.last + 1; i < len; i++){
				start = bs[i];
				if(start < 0)continue;
				//loop through the history for this planet
				for(var i2 = 0, idx2 = start, idy2 = idx2 + 1; i2 < histLen; i2++, idx2 += 2, idy2 += 2){
					//update the point
					tpts[idx2] = e[0] * posHist[idx2] + e[3] * posHist[idy2] + e[6];
					tpts[idy2] = e[1] * posHist[idx2] + e[4] * posHist[idy2] + e[7];
				}
				//clear out the ptsNeedUpdate array as you go
				pnu[i] = -1;
			}
			this.changed = false;
			_history.needsUpdateLatest = false;
			_history.needsUpdateAll = false;
		}
		else if(_history.needsUpdateLatest){
			var idx = 0, idy = 0, posx = 0, posy = 0;
			for(var i = 0, len = _history.last + 1; i < len; i++){
				//get the index of the next point that needs updating
				idx = pnu[i];
				if(idx < 0)continue;	//this shouldn't ever happen
				idy = idx + 1;
				//transform the point
				//store it in the transformed points array
				tpts[idx] = e[0] * posHist[idx] + e[3] * posHist[idy] + e[6];
				tpts[idy] = e[1] * posHist[idx] + e[4] * posHist[idy] + e[7];
				//clear out the flagged index
				pnu[i] = -1;
			}
			_history.needsUpdateLatest = false;
		}
	},
	//pass in an array of positions and radii
	//pass in an array for the results to be stored
	//optionally pass in the number of indices to check
	//performs the visibility check on each planet
	/*values are encoded in the following manner:
	1010 | 0010 | 0110
	------------------
	1000 | 0000 | 0100
	------------------
	1001 | 0001 | 0101
	Where the region labelled 0000 is the viewport and the other regions are outside the viewport
	
	If a planet is invalid, the value is set to 15 (0b1111)
	*/
	checkVisibility: function(_state){
		var num = _state.numPlanets,
		positions = _state.positions,
		radii = _state.radii,
		visibility = _state.visibility;
		var numVisible = 0;
		
		var width = this.width, height = this.height;
		var posx = 0, posy = 0, radius = 0, visible = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1, count = 0, len = radii.length; i < len && count < num; i++, idx += 2, idy += 2){
			visibility[i] = 15; //0b1111
			radius = radii[i];
			if(radius < 0)continue;
			
			count++;
			
			posx = positions[idx];
			posy = positions[idy];
			
			visible = 0; //0b0000
			//check for visibility
			if(posx + radius < 0)
				visible = visible | 8; //0b1000
			if(posx - radius > width)
				visible = visible | 4; //0b0100
			if(posy + radius < 0)
				visible = visible | 2; //0b0010
			if(posy - radius > height)
				visible = visible | 1; //0b0001
			
			visibility[i] = visible;
			if(visible === 0) //0b0000
				numVisible++;
		}
		return numVisible;
	},
	//project a world-space point to screen space 
	project: function(_pos){
		//TODO
	},
	//project a screen-space point to world space
	unprojectVector: function(_pos){
		//un-shift by the viewport dimensions
		//un-translate
		//un-scale
		//un-rotate
		_pos.applyMatrix3(this.invMatrixWorld);
	}
};


/*
Transform from world space to camera space:
Rotate
Scale
Translate

Transform from camera space to screen space:
Shift by the viewport dimensions



*/















