/**
*	Engine3D.js
*	6/8/2015
*/

GRAV.Engine3D = function(){
	//
	//
};

GRAV.Engine3D.prototype = {
	constructor: GRAV.Engine3D,
	planets: 0,
	substeps: 2,
	timescale: 1.0,
	timestep: 0.5,
	type: 'Engine',
	//real G is 0.0000000000667384
	//6.67384 * 10 ^ -11
	G: 0.1,
	maxPlanets: 0,
	capacity: 0,
	stepNum: 0,
	newPlanetCount: 0,
	destrPlanetCount: 0,
	randomStream: undefined,
	history: undefined,
	disabledHistory: undefined,
	setSubsteps: function(_num){
		this.substeps = Math.max(1, _num);
		this.setTimescale(this.timescale);
	},
	getSubsteps: function(){
		return this.substeps;
	},
	setTimescale: function(_fac){
		this.timescale = Math.max(0, _fac);
		this.timestep = this.timescale / this.substeps;
	},
	getTimescale: function(){
		return this.timescale;
	},
	step: function(){
		var i = this.substeps;
		//console.log('step');
		this.stepNum++;
		while(i--){
			//console.log('substep');
			this._substep();
		}
	},
	_substep: function(){
		console.warn('GRAV.Engine3D.step: Not implemented in base class.');
	},
	addPlanet: function(){
		console.warn('GRAV.Engine3D.addPlanet: Not implemented in base class.');
	},
	clear: function(){
		console.warn('GRAV.Engine3D.clear: Not implemented in base class.');
	},
	randomize: function(){
		console.warn('GRAV.Engine3D.randomize: Not implemented in base class.');
	},
	getState: function(){
		console.warn('GRAV.Engine3D.getState: Not implemented in base class.');
	},
	loadState: function(){
		console.warn('GRAV.Engine3D.loadState: Not implemented in base class.');
	},
	getTrajectory: function(){
		console.warn('GRAV.Engine3D.calculateTrajectory: Not implemented in base class.');
	},
	createHistory: function(){
		console.warn('GRAV.Engine3D.createHistory: Not implemented in base class.');
	},
	useHistory: function(){
		console.warn('GRAV.Engine3D.useHistory: Not implemented in base class.');
	},
	updateHistory: function(){
		console.warn('GRAV.Engine3D.updateHistory: Not implemented in base class.');
	},
	numPlanets: function(){
		return this.planets;
	},
	maxSize: function(){
		console.warn('GRAV.Engine3D.maxSize: Not implemented in base class.');
	},
	getCapacity: function(){
		return this.capacity;
	},
	planetsAtPt: function(){
		console.warn('GRAV.Engine3D.planetsAtPt: Not implemented in base class.');
	},
	getPlanet: function(){
		console.warn('GRAV.Engine3D.getPlanet: Not implemented in base class.');
	}
};

GRAV.CPUEngine3D = function(_params){
	GRAV.Engine3D.call(this);
	this.type = 'CPUEngine3D';
	
	_params = _params || {};
	
	this.capacity = 	_params.capacity 	|| 1024;	//this can be changed by calling this.resize() but shouldn't be greater than this.maxPlanets
	this.substeps = 	_params.substeps 	|| this.substeps;
	this.timescale = 	_params.timescale 	|| this.timescale;
	this.G = 			_params.G 			|| this.G;
	this.maxPlanets = 32767;	//this is because the collision table is an array of 16-bit ints. If more planets are required, we could switch it to 32-bit ints, giving a maximum of 2,147,483,647. We can't use Uint16 since we sometimes need to store a -1. This is unfortunate since we're wasting nearly half the integer range.
	var useHist = _params.useHistory !== undefined? _params.useHistory : true;
	
	this.randomStream = new Float32Array(this.capacity * 3);
	
	this.setTimescale(this.timescale);
	
	this.state = {
		position: new Float64Array(this.capacity * 3),	//the state needs to be as accurate as possible
		velocity: new Float64Array(this.capacity * 3),
		mass: new Float64Array(this.capacity),
		radius: new Float64Array(this.capacity),
		collision: new Int16Array(this.capacity),	//this would impose a hard maximum of 32767 planets
		id: new Int16Array(this.capacity),	//this is the public-facing ID. It doesn't change and ids are not recycled during a single simulation run
		indexOf: new Int16Array(this.capacity * 2),	//Each entry contains the defragmented location of the planet with that index. It's a non-defragmented array. The capacity of this array might limit the total number of possible planets, dead or alive
		destroyedPlanets: new Int16Array(this.capacity),	//a running list of all planets destroyed  and created since the last call to .getState()
		newPlanets: new Int16Array(Math.ceil(this.capacity / 4))
	};
	
	//by default, create a history
	this.createHistory();
	if(!useHist){
		this.useHistory(false);
	}
	
	//initialize all planets to non-existent
	this.clear();
};

GRAV.CPUEngine3D.prototype = Object.create(GRAV.Engine3D.prototype);
GRAV.CPUEngine3D.prototype.constructor = GRAV.CPUEngine3D;

GRAV.CPUEngine3D.prototype._substep = function(){
	//console.log('did a CPU substep');
	//find all collisions
	if(this._findCollisions() > 0){
		//resolve collisions
		if(this._resolveCollisions() > 0){
			//defragment if there were collisions
			this._defragment();
		}
	}
	//update velocity
	this._updateVelocity();
	//update position
	this._updatePosition();
};

GRAV.CPUEngine3D.prototype._findCollisions = function(){
	var sc = this.state.collision, 
	sp = this.state.position,
	sr = this.state.radius;
	
	var curX = 0, curY = 0, curZ = 0, curRad = 0,
	otherX = 0, otherY = 0, otherZ = 0, otherRad = 0;
	var collision_maxRad = -1,
	collision_idx = -1,
	dist = 0,
	possible_collisions = 0;
	//for each planet find the biggest object it collides with
	for(var i = 0, len = this.last + 1, idx = i * 3, idy = idx + 1, idz = idy + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		curRad = sr[i];
		sc[i] = -1;
		if(curRad < 0)continue;	//it's not a valid planet
		curX = sp[idx];
		curY = sp[idy];
		curZ = sp[idz];
		collision_maxRad = -1;
		collision_idx = -1;
		//for each other planet
		for(var i2 = 0, len2 = this.last + 1, idx2 = i2 * 3, idy2 = idx2 + 1, idz2 = idy2 + 1; i2 < len2; i2++, idx2 += 3, idy2 += 3, idz2 += 3){
			if(i === i2)continue;
			otherRad = sr[i2];
			//if the other planet is smaller, skip it. (this also handles the case that the other radius is -1)
			if(otherRad < curRad)continue;
			
			otherX = sp[idx2];
			otherY = sp[idy2];
			otherZ = sp[idz2];
			
			//if their bounding boxes don't overlap, they definitely won't collide
			if(Math.abs(curX - otherX) * 2 > (curRad + curRad + otherRad + otherRad))continue;
			if(Math.abs(curY - otherY) * 2 > (curRad + curRad + otherRad + otherRad))continue;
			if(Math.abs(curZ - otherZ) * 2 > (curRad + curRad + otherRad + otherRad))continue;

			//get the distance between the two planets
			dist = Math.sqrt(((otherX - curX) * (otherX - curX)) +
							((otherY - curY) * (otherY - curY)) +
							((otherZ - curZ) * (otherZ - curZ)));
			//if they don't collide, continue
			if(dist > curRad + otherRad)continue;
			//if the other planet's mass is bigger than the current largest collision
			if(otherRad > collision_maxRad){
				//if they're the same size, we'll say that the one who comes last is smaller
				if(curRad === otherRad && i2 > i)continue;
				//store the other planet's mass as the biggest collision
				collision_maxRad = otherRad;
				//store the other planet's index
				collision_idx = i2;
				possible_collisions++;
			}
		}
		//store the index of the colliding larger planet
		sc[i] = collision_idx;
	}
	
	return possible_collisions;
};

GRAV.CPUEngine3D.prototype._resolveCollisions = function(){
	var sp = this.state.position,
	sv = this.state.velocity,
	sm = this.state.mass,
	sr = this.state.radius,
	sc = this.state.collision
	si = this.state.id
	sio = this.state.indexOf,
	sdp = this.state.destroyedPlanets;
	var hist = this.history;
	var histLen3 = 1;
	if(hist !== undefined){
		histLen3 = hist.historyLength * 3;
	}
	var numCollisions = 0;
	var pColl = 0, pid = 0;
	var dp = this.destrPlanetCount,
	numCol = this.numCollisions,
	numPlan = this.planets;
	//for each planet
	for(var i = 0, len = this.last + 1; i < len; i++){
		pColl = 0;
		//if it collides with a bigger planet &&
		//if the bigger planet doesn't collide with anything else
		pid = sc[i];
		if(pid < 0)continue;
		//count the length of the collision chain
		while(sc[pid] >= 0){
			pColl++;
			pid = sc[pid];
		}
		//if(sc[i] >= 0 && sc[sc[i]] < 0){
		//collide if the parent has an even number of parent collisions
		if(pColl % 2 === 0){
			sdp[dp] = sio[si[i]];	//add this planet to the list of destroyed ones
			//add this planet's mass and velocity into the bigger planet
			absorbPlanet(sc[i], i);
			
			numCollisions++;
			numCol++;
			dp++;
			numPlan--;
		}
	}
	this.destrPlanetCount = dp;
	this.numCollisions = numCol;
	this.planets = numPlan;
	//clear out the collision entries
	for(var i = 0, len = this.last + 1; i < len; i++){
		sc[i] = -1;
	}
	
	//if there were collisions
	if(numCollisions > 0){
		var last = this.last;
		//update this.last
		while(sm[last] < 0 && last >= 0){
			last--;
		}
		this.last = last;
	}
	
	return numCollisions;
	
	//planet A absorbs planet B
	function absorbPlanet(_i_a, _i_b){
		var mass_a = sm[_i_a], mass_b = sm[_i_b];
		var newMass = mass_a + mass_b;
		//get the ratio of the planet masses to the combined mass
		var mass_ratio_a = mass_a / newMass,
		mass_ratio_b = 1 - mass_ratio_a;
		//get the indices into the position and velocity arrays
		var idx_a = _i_a * 3, idy_a = _i_a * 3 + 1, idz_a = _i_a * 3 + 2,
		idx_b = _i_b * 2, idy_b = _i_b * 2 + 1, idz_b = _i_b * 3 + 2;
		//calculate the new position
		sp[idx_a] = (sp[idx_a] * mass_ratio_a) + (sp[idx_b] * mass_ratio_b);
		sp[idy_a] = (sp[idy_a] * mass_ratio_a) + (sp[idy_b] * mass_ratio_b);
		sp[idz_a] = (sp[idz_a] * mass_ratio_a) + (sp[idz_b] * mass_ratio_b);
		//calculate the new velocity
		sv[idx_a] = (sv[idx_a] * mass_ratio_a) + (sv[idx_b] * mass_ratio_b);
		sv[idy_a] = (sv[idy_a] * mass_ratio_a) + (sv[idy_b] * mass_ratio_b);
		sv[idz_a] = (sv[idz_a] * mass_ratio_a) + (sv[idz_b] * mass_ratio_b);
		
		//store the new mass
		sm[_i_a] = newMass;
		//calculate the new radius
		sr[_i_a] = Math.pow( ( newMass * PI3_4 ) , 0.3333);
		//clear out planet B
		sm[_i_b] = -1;
		sr[_i_b] = -1;
		sio[si[_i_b]] = -1;
		si[_i_b] = -1;
		sp[idx_b] = 0;
		sp[idy_b] = 0;
		sp[idz_b] = 0;
		sv[idx_b] = 0;
		sv[idy_b] = 0;
		sv[idz_b] = 0;
		
		//if there's a history
		if(hist !== undefined){
			//inform the History that the block is now free
			hist.blockOwner[hist.blockStart[_i_b] / histLen3] = -1;
			//clear out the entry
			hist.blockStart[_i_b] = -1;
		}
	}
};

GRAV.CPUEngine3D.prototype._updateVelocity = function(){
	var sm = this.state.mass,
	sp = this.state.position,
	sv = this.state.velocity;
	
	//variables to be used within the loop
	var curX = 0, curY = 0, curZ = 0, curMass = 0,
	otherX = 0, otherY = 0, otherZ = 0, otherMass = 0,
	dist = 0, dist2 = 0;
	var vx = 0, vy = 0, vz = 0, ux = 0, uy = 0, uz = 0, acc = 0;	//more variables to be used within the loop
	var G = this.G;	//gravitational constant
	for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1, len = this.last + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		curMass = sm[i];
		if(curMass < 0)continue;
		//get the current planet's info
		curX = sp[idx];
		curY = sp[idy];
		curZ = sp[idz];
		//By updating both the current planet and the other planet, 
		// I can cut the number of Math.sqrt() calls in half
		for(var i2 = i + 1, idx2 = i2 * 3, idy2 = idx2 + 1, idz2 = idy2 + 1, len2 = this.last + 1; i2 < len2; i2++, idx2 += 3, idy2 += 3, idz2 += 3){
			//if it's the same planet, skip it
			//if(i2 === i)continue;	//I don't think this is necessary anymore
			
			otherMass = sm[i2];
			if(otherMass < 0)continue;	//if the planet is invalid
			
			otherX = sp[idx2];
			otherY = sp[idy2];
			otherZ = sp[idz2];
			
			vx = otherX - curX;
			vy = otherY - curY;
			vz = otherZ - curZ;
			
			//dist = _distance(curX, curY, otherX, otherY);
			dist2 = (vx * vx) + (vy * vy) + (vz * vz);
			dist = Math.sqrt(dist2);

			//update this planet's acceleration
			ux = vx / dist;
			uy = vy / dist;
			uz = vz / dist;
			acc = (G * otherMass) / dist2;
			
			sv[idx] += ux * acc;
			sv[idy] += uy * acc;
			sv[idz] += uz * acc;
			
			//update the other planet's acceleration
			vx = curX - otherX;
			vy = curY - otherY;
			vz = curZ - otherZ;
			
			ux = vx / dist;
			uy = vy / dist;
			uz = vz / dist;
			acc = (G * curMass) / dist2;
			
			sv[idx2] += ux * acc;
			sv[idy2] += uy * acc;
			sv[idz2] += uz * acc;
		}		
	}
};

GRAV.CPUEngine3D.prototype._updatePosition = function(){
	var sp = this.state.position;
	var sv = this.state.velocity;
	
	var dt = this.timestep;
	for(var i = 0, len = this.last + 1, idx = i * 3, idy = idx + 1, idz = idy + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		sp[idx] += sv[idx] * dt;
		sp[idy] += sv[idy] * dt;
		sp[idz] += sv[idz] * dt;
	}
};

//_out should be at least as long as the number of iterations * 2
GRAV.CPUEngine3D.prototype.getTrajectory = function(_x, _y, _z, _velX, _velY, _velZ, _mass, _out, _numIterations){	//once we find a good number of iterations, we can probably hard-code it
	//given a new planet (position, mass, velocity), calculate its trajectory for a few iterations
	//Make the timestep really large, like 10x the regular timestep. This is supposed to be a rough estimate, not a precise calculation
	var sm = this.state.mass,
	sp = this.state.position;
	var fastStep = this.timestep * 10;
	var prevX = _x, prevY = _y, prevZ = _z,
	otherX = 0, otherY = 0, otherZ = 0,
	otherMass = 0, dist = 0, dist2 = 0;
	
	var vx = 0, vy = 0, vz = 0, ux = 0, uy = 0, uz = 0, acc = 0;	//helper variables
	for(var it = 0, itx = it * 3, ity = itx + 1, itz = ity + 1; it < _numIterations; it++, itx += 3, ity += 3, itz += 3){
		//calculate the new velocity for this iteration
		for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1, len = this.last + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
			otherMass = sm[i];
			if(otherMass < 0)continue;
			
			otherX = sp[idx];
			otherY = sp[idy];
			otherZ = sp[idz];
			
			dist2 = (otherX - _x) * (otherX - _x) + (otherY - _y) * (otherY - _y) + (otherZ - _z) * (otherZ - _z);
			dist = Math.sqrt(dist2);
			
			vx = otherX - _x;
			vy = otherY - _y;
			vz = otherZ - _z;
			
			ux = vx / dist;
			uy = vy / dist;
			uz = vz / dist;
			acc = (G * otherMass) / dist2;
			
			_velX += ux * acc;
			_velY += uy * acc;
			_velZ += uz * acc;
		}
		//apply the velocity
		prevX += _velX * fastStep;
		prevY += _velY * fastStep;
		prevZ += _velZ * fastStep;
		_out[itx] = prevX;
		_out[ity] = prevY;
		_out[itz] = prevZ;
	}
};

GRAV.CPUEngine3D.prototype.clear = function(){
	var sm = this.state.mass, sr = this.state.radius,
	sp = this.state.position, sv = this.state.velocity,
	sc = this.state.collision, si = this.state.id,
	sio = this.state.indexOf, sdp = this.state.destroyedPlanets,
	snp = this.state.newPlanets;
	
	//these ones have length this.capacity
	for(var i = 0, len = sm.length; i < len; i++){
		sm[i] = -1;
		sr[i] = -1;
		sc[i] = -1;
		si[i] = -1;
		sdp[i] = -1;
	}
	
	for(var i = 0, len = snp.length; i < len; i++){
		snp[i] = -1;
	}
	
	//these ones have length this.capacity * 2
	for(var i = 0, len = sp.length; i < len; i++){
		sp[i] = 0;
		sv[i] = 0;
	}
	
	for(var i = 0, len = sio.length; i < len; i++){
		sio[i] = -1;
	}
	
	if(this.history !== undefined){
		this.history.clear();
	}
	
	this.next = 0;
	this.nextID = 0;
	this.last = -1;
	
	this.numCollisions = 0;
	this.newPlanetCount = 0;
	this.destrPlanetCount = 0;
	this.planets = 0;
	this.defragCount = 0;
};

GRAV.CPUEngine3D.prototype.randomize = function(_params){
	_params = _params || {};
	
	var num = _params.num || Math.floor(this.capacity * 0.5);
	var xbound = _params.xbound || 800;
	var ybound = _params.ybound || 800;
	var zbound = _params.zbound || 800;
	var minMass = _params.minMass || 5;
	var maxMass = _params.maxMass || 400;
	var maxVel = _params.maxVel || 0;
	var elliptical = _params.elliptical || false;
	
	this.clear();
	
	var rand = this.randomStream;
	if(elliptical && false){
		//TODO: change this from a circle to a sphere
		var xrange = xbound * 0.5;
		var yrange = ybound * 0.5;
		var zrange = zbound * 0.5;
		var angle = 0, radius = 0;
		for(var i = 0, idx = i * 2, idy = idx + 1; i < num; i++, idx += 2, idy += 2){
			angle = Math.random() * TWOPI;
			radius = Math.sqrt(Math.random());
			rand[idx] = Math.cos(angle) * radius * xrange;
			rand[idy] = Math.sin(angle) * radius * yrange;
		}
	}
	else{
		for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1; i < num; i++, idx += 3, idy += 3, idz += 3){
			rand[idx] = Math.random() * xbound - (xbound * 0.5);
			rand[idy] = Math.random() * ybound - (ybound * 0.5);
			rand[idz] = Math.random() * zbound - (zbound * 0.5);
		}
	}
	
	var sp = this.state.position,
	sm = this.state.mass,
	sr = this.state.radius,
	sv = this.state.velocity
	si = this.state.id,
	sio = this.state.indexOf;
	
	for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1; i < num; i++, idx += 3, idy += 3, idz += 3){
		sm[i] = Math.random() * (maxMass - minMass) + minMass;
		sr[i] = Math.pow( ( sm[i] * PI3_4 ), 0.3333);
		sp[idx] = rand[idx];
		sp[idy] = rand[idy];
		sp[idz] = rand[idz];
		sv[idx] = Math.random() * maxVel * 2 - maxVel;
		sv[idy] = Math.random() * maxVel * 2 - maxVel;
		sv[idz] = Math.random() * maxVel * 2 - maxVel;
		si[i] = this.nextID++;
		sio[i] = i;
	}
	this.next = num;
	this.last = num - 1;
	
	//initialize the history
	if(this.history !== undefined){
		this.history.fill(sp, num);
	}
	
	this.planets = num;
};

GRAV.CPUEngine3D.prototype._defragment = function(){
	var hasHistory = this.history !== undefined;
	var bs = undefined, ts = undefined, bo = undefined,
	histLen = 0;
	if(hasHistory){
		bs = this.history.blockStart;
		ts = this.history.tailStart;
		bo = this.history.blockOwner;
		histLen2 = this.history.historyLength * 3;
	}
	//restructure the contents of the state arrays so that there are no gaps
	var sm = this.state.mass,
	sp = this.state.position,
	sv = this.state.velocity,
	sr = this.state.radius,
	si = this.state.id,
	sio = this.state.indexOf;
	var next = 0;
	//find the first available spot
	while(sm[next] > -1 && next < this.capacity){
		next++;
	}
	
	for(var i = next, len = this.last + 1, idx = i * 3, idy = idx + 1, idz = idy + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		//if there's a planet here
		if(sm[i] > 0){
			//move it down to the next slot
			sm[next] = sm[i];
			sr[next] = sr[i];
			si[next] = si[i];
			sp[next * 3] = sp[idx];
			sp[next * 3 + 1] = sp[idy];
			sp[next * 3 + 2] = sp[idz];
			sv[next * 3] = sv[idx];
			sv[next * 3 + 1] = sv[idy];
			sv[next * 3 + 2] = sv[idz];
			
			//update indexOf
			sio[si[next]] = next;
			
			//clear the old slots
			sm[i] = -1;
			sr[i] = -1;
			si[i] = -1;
			sp[idx] = 0;
			sp[idy] = 0;
			sp[idz] = 0;
			sv[idx] = 0;
			sv[idy] = 0;
			sv[idz] = 0;
			
			//if there is a history, defragment its blockStart, blockOwner, and tailStart arrays
			if(hasHistory){
				bo[bs[i] / histLen2] = next;
				bs[next] = bs[i];
				ts[next] = ts[i];
				
				bs[i] = -1;
				ts[i] = -1;
			}
			//update 'next'
			while(sm[next] > -1 && next < this.capacity){
				next++;
			}
		}
	}
	this.last = next - 1;
	this.next = next;
	if(hasHistory){
		this.history.last = this.last;
	}
	this.defragCount++;
};

//maybe call resize if the arrays are not being adequately filled, like if there are fewer than 1/16th the elements being used
GRAV.CPUEngine3D.prototype._resize = function(_newMax){
	//TODO
	console.log('resized');
	//should we even allow this? This would make some things kinda hard
};

GRAV.CPUEngine3D.prototype.addPlanet = function(_x, _y, _z, _velx, _vely, _velz, _mass){
	if(this.planets === this.capacity){
		console.log('The simulation has reached maximum capacity!');
		return;
	}
	//call resize if necessary
	/*if(this.planets >= this.capacity - 1){
		this._resize();
	}*/
	//put it in the next empty slot
	var st = this.state;
	var n = this.next;
	st.mass[n] = _mass;
	st.radius[n] = Math.pow( ( _mass * PI3_4 ) , 0.3333);
	st.position[n * 3] = _x;
	st.position[n * 3 + 1] = _y;
	st.position[n * 3 + 2] = _z;
	st.velocity[n * 3] = _velx;
	st.velocity[n * 3 + 1] = _vely;
	st.velocity[n * 3 + 2] = _velz;
	st.id[n] = this.nextID++;
	st.indexOf[st.id[n]] = n;
	st.newPlanets[this.newPlanetCount] = st.id[n];
	//update the history, if any
	if(this.history !== undefined){
		this.history.addPlanet(_x, _y, _z, n);
	}
	//find the new next empty slot
	while(st.mass[n % this.capacity] > 0){
		n++;
	}
	this.next = n;
	this.planets++;
	this.newPlanetCount++;
	//update this.last
	this.last = n - 1;
};

GRAV.CPUEngine3D.prototype.createHistory = function(_params){
	_params = _params || {};
	this.history = new GRAV.HistoryManager({
		capacity: this.capacity,
		historyLength: _params.historyLength,
		skip: _params.skip,
		dimensions: 3
	});
};

//returns whether or not the setting was actually changed
GRAV.CPUEngine3D.prototype.useHistory = function(_use){
	if(_use === undefined)_use = true;
	
	//if this is already enabled/disabled, don't do it again
	if(_use == (this.history !== undefined))return false;
	
	if(_use){
		this.history = this.disabledHistory;
		this.disabledHistory = undefined;
	}
	else{
		this.disabledHistory = this.history;
		this.history = undefined;
	}
	return true;
};

GRAV.CPUEngine3D.prototype.updateHistory = function(){
	var hist = this.history;
	if(hist == undefined || !hist.needsUpdate(this.stepNum))return;
	var ts = hist.tailStart,
	bs = hist.blockStart,
	posHist = hist.history,
	update = hist.ptsNeedUpdate,
	histLen = hist.historyLength;
	var histLen3 = histLen * 3;
	var sp = this.state.position,
	sm = this.state.mass;
	//add lastest positions to the position history
	//update the history's frame number
	var curTail = 0, newTail = 0, curBlock = 0;
	
	for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1, len = this.last + 1, count = 0; i < len; i++, idx += 3, idy += 3, idz += 3){
		if(sm[i] < 0)continue;
		//get the block position
		curBlock = bs[i];
		//get the current tail position
		curTail = ts[i];
		//find the new tail position
		newTail = ((((curTail - 3) % histLen3) + histLen3) % histLen3);	//this weirdness ensures it's positive
		//store the new position
		posHist[curBlock + newTail] = sp[idx];
		posHist[curBlock + newTail + 1] = sp[idy];
		posHist[curBlock + newTail + 2] = sp[idz];
		//set the update flag
		//update[(curBlock >> 1) + (newTail >> 1)] = 1;	//this has a bit-shift since it's half the size
		update[count] = curBlock + newTail;
		//store the new tail position
		ts[i] = newTail;
		count++;
		hist.changed = true;
	}
};

GRAV.CPUEngine3D.prototype.maxSize = function(){
	return this.maxPlanets;
};

//make sure the arrays you pass in are big enough to fit all the planets!!!
GRAV.CPUEngine3D.prototype.getState = function(_state){
	var ts = this.state;
	//copy the planet positions and their radii into the arrays provided by the caller
	_state.positions.set(ts.position);
	_state.radii.set(ts.radius);
	_state.velocities.set(ts.velocity);
	_state.numPlanets = this.planets;
	_state.matrixApplied = false;
	
	//include the IDs of destroyed planets and new planets
	_state.created.set(ts.newPlanets);
	_state.destroyed.set(ts.destroyedPlanets);
	//clear out these arrays
	for(var i = 0, len = this.newPlanetCount; i < len; i++){
		ts.newPlanets[i] = -1;
	}
	this.newPlanetCount = 0;
	for(var i = 0, len = this.destrPlanetCount; i < len; i++){
		ts.destroyedPlanets[i] = -1;
	}
	this.destrPlanetCount = 0;
	
	//return the number of planets in the array
	return this.planets;
};

GRAV.CPUEngine3D.prototype.loadState = function(_state){
	//TODO
};

GRAV.CPUEngine3D.prototype.planetsAtPt = function(_x, _y, _z, _out){
	var count = 0, outIdx = 0, outSize = _out.length;
	var sp = this.state.position,
	sr = this.state.radius,
	sm = this.state.mass,
	si = this.state.id;
	var posx = 0, posy = 0, posz = 0, rad = 0, dist = 0;
	//find all the planets who encompass the point [_x, _y]
	for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1, len = this.last + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		if(sm[i] < 0)continue;
		posx = sp[idx];
		posy = sp[idy];
		posz = sp[idz];
		rad = sr[i];
		
		if(_x < posx - rad)continue;	//too far left
		if(_x > posx + rad)continue;	//too far right
		if(_y < posy - rad)continue;	//too far up
		if(_y > posy + rad)continue;	//too far down
		if(_z < posz - rad)continue;	//too far forward
		if(_z > posz + rad)continue;	//too far backward
		
		//get the actual distance
		dist = Math.sqrt(((_x - posx) * (_x - posx)) + ((_y - posy) * (_y - posy)) + ((_z - posz) * (_z - posz)));
		if(dist > rad)continue;
		
		//we've found one
		if(outIdx < outSize){
			_out[outIdx] = si[i];
			outIdx++;
		}
		count++;
	}
	//return the number selected
	return count;
};

GRAV.CPUEngine3D.prototype.getPlanet = function(_id, _out){
	_out = _out || {};
	var st = this.state
	sm = st.mass,
	si = st.id,
	sio = st.indexOf;
	var idx = sio[_id];
	if(idx !== -1 && !isNaN(idx)){
		_out.x = st.position[idx * 3];
		_out.y = st.position[idx * 3 + 1];
		_out.z = st.position[idx * 3 + 2];
		_out.velx = st.velocity[idx * 3];
		_out.vely = st.velocity[idx * 3 + 1];
		_out.velz = st.velocity[idx * 3 + 2];
		_out.mass = sm[idx];
		_out.radius = st.radius[idx];
	}
	return _out;
};

//Debugging functions
GRAV.CPUEngine3D.prototype.distanceBetween = function(_a, _b){
	var x1 = this.state.position[_a * 3];
	var y1 = this.state.position[_a * 3 + 1];
	var z1 = this.state.position[_a * 3 + 2];
	
	var x2 = this.state.position[_b * 3];
	var y2 = this.state.position[_b * 3 + 1];
	var z2 = this.state.position[_b * 3 + 2];
	var one = x2 - x1;
	var two = y2 - y1;
	var thr = z2 - z1;
	
	return Math.sqrt((one * one) + (two * two) + (thr * thr));
};

GRAV.CPUEngine3D.prototype.totalMass = function(){
	var mass = 0;
	var sm = this.state.mass;
	for(var i = 0; i < this.capacity; i++){
		if(sm[i] > 0){
			mass += sm[i];
		}
	}
	return mass;
};

GRAV.CPUEngine3D.prototype.totalEnergy = function(){
	//E = 0.5 * m * v^2
	var sm = this.state.mass,
	sv = this.state.velocity;
	var totalEnergy = 0,
	curEnergy = 0,
	curVel2 = 0;
	for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1, len = this.last + 1; i < len; i++, idx += 3, idy += 3, idz += 3){
		if(sm[i] < 0)continue;
		curVel2 = (sv[idx] * sv[idx]) + (sv[idy] * sv[idy]) + (sv[idz] * sv[idz]);
		curEnergy = 0.5 * sm[i] * curVel2;
		totalEnergy += curEnergy;
	}
	return totalEnergy;
};

GRAV.CPUEngine3D.prototype.test = function(_params){
	_params = _params || {};
	
	var iterations = _params.iterations || 1000;
	var trials = _params.trials || 20;
	var useHistory = _params.useHistory === undefined ? false : _params.useHistory;
	
	var trialRuns = new Float64Array(trials);
	var start = 0, end = 0, i = trials;
	var randSettings = {
		num: 512,
		xbound: 800,
		ybound: 800,
		zbound: 800,
		minMass: 5,
		maxMass: 400,
		maxVel: 0,
		elliptical: false
	};
	var historyBackup = this.history;
	var changed = this.useHistory(useHistory);
		
	for(var i = 0; i < trials; i++){
		this.randomize(randSettings);
		start = performance.now();
		for(var i2 = 0; i2 < iterations; i2++){
			this.step();
		}
		end = performance.now();
		trialRuns[i] = end - start;
	}
	if(changed){
		this.useHistory(!useHistory);
	}
	
	var sum = 0;
	for(var i = 0; i < trials; i++){
		sum += trialRuns[i];
	}
	var average = sum / trials;
	console.log('' + average + ' ms');
	alert('done');
};












