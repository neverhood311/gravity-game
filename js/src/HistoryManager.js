/**
*	HistoryManager.js
*	5/31/2015
*
*	The arrays are laid out as follows: (trail length of 2)
*
*	history (two coordinates for each history position for each planet):
*	| plnt1.slot1.x | plnt1.slot1.y | plnt1.slot2.x | plnt1.slot2.y | plnt2.slot1.x | plnt2.slot1.y | ...
*
*	blockStart (one slot for each planet, indexes into this.history)
*	| planet1 | planet2 | planet3 | ...
*
*	tailStart (one slot for each planet, indexes into a history for one planet)
*	| planet1 | planet2 | planet3 | ...
*
*	transformedPts (two coordinates for each history position for each planet):
*	| plnt1.slot1.x | plnt1.slot1.y | plnt1.slot2.x | plnt1.slot2.y | plnt2.slot1.x | plnt2.slot1.y | ...
*
*	ptsNeedUpdate (as many slots as there are planets, indexes into history):
*	| idx1 | idx2 | idx3 | ...
*/

GRAV.HistoryManager = function(_params){
	_params = _params || {};
	
	//history length
	this.historyLength = Math.min(_params.historyLength, 256) || 100;
	//how many steps to skip between data points
	this.skip = _params.skip || 1;
	//engine capacity
	this.capacity = _params.capacity || 1024;
	
	this.dimensions = _params.dimensions || 2;
	
	this.init();
	
	this.clear();
};

GRAV.HistoryManager.prototype = {
	constructor: GRAV.HistoryManager,
	historyLength: 50,
	skip: 5,
	capacity: 1024,
	next: 0,	//the next available slot for storing a history
	last: -1,
	frame: 0,
	needsUpdateLatest: true,
	needsUpdateAll: true,
	history: undefined,
	blockStart: undefined,
	tailStart: undefined,
	transformedPts: undefined,
	ptsNeedUpdate: undefined,
	blockOwner: undefined,
	addPlanet: function(_x, _y, _idx){
		//find the next history slot
		var bs = this.blockStart,
		hist = this.history;
		var n = this.next, cap = this.capacity;
		while(this.blockOwner[n % cap] > -1){
			n++;
		}
		if(this.dimensions === 2){
			bs[_idx] = n * 2 * this.historyLength;
			this.tailStart[_idx] = 0;
			this.blockOwner[_idx] = _idx;
			for(var i = 0, len = this.historyLength, idx = bs[_idx], idy = idx + 1; i < len; i++, idx += 2, idy += 2){
				hist[idx] = _x;
				hist[idy] = _y;
			}
			this.ptsNeedUpdate[_idx] = -1;
		}
		this.last = n;
		this.next = n + 1;
		this.needsUpdateLatest = true;
		this.needsUpdateAll = true;
	},
	removePlanet: function(_idx){
		this.blockStart[_idx] = -1;
		this.tailStart[_idx] = 0;	//this one is optional
		this.blockOwner[_idx] = -1;
		this.ptsNeedUpdate[_idx] = -1;
	},
	needsUpdate: function(_frame){
		return _frame % this.skip === 0;
	},
	clear: function(){
		var bs = this.blockStart,
		pnu = this.ptsNeedUpdate,
		bo = this.blockOwner;
		
		//clear out the blockStart array
		//clear out the ptsNeedUpdate array
		for(var i = 0, len = this.capacity; i < len; i++){
			bs[i] = -1;
			pnu[i] = -1;
			bo[i] = -1;
		}
		this.last = -1;
		this.next = 0;
		this.needsUpdateLatest = true;
	},
	init: function(){
		//history
		this.history = new Float32Array(this.capacity * this.historyLength * this.dimensions);	//S_2*H_data
		//pointers into history array. This will need to be defragmented
		this.blockStart = new Int32Array(this.capacity);		//D_1_ID2*H
		this.tailStart = new Int16Array(this.capacity);			//D_1_ID2*H
		
		//transformed points
		this.transformedPts = new Float32Array(this.capacity * this.historyLength * this.dimensions);
		//which planets need to be updated and which point in the history needs the update
		//an index into this array corresponds to the same index for the planet in the Mass array
		//the values of this array refer to the index of the x-value of the position within the this.history array
		this.ptsNeedUpdate = new Int32Array(this.capacity);
		//who owns each history block
		this.blockOwner = new Int16Array(this.capacity);
		this.needsUpdateAll = true;
	},
	fill: function(_positions, _num){
		var posHist = this.history,
		bs = this.blockStart,
		ts = this.tailStart,
		pnu = this.ptsNeedUpdate
		bo = this.blockOwner,
		histLen = this.historyLength;
		if(this.dimensions === 2){
			for(var i = 0, idx = i * 2, idy = idx + 1; i < _num; i++, idx += 2, idy += 2){
				bs[i] = i * histLen * 2;
				ts[i] = 0;
				bo[i] = i;
				//fill in the history with the initial position
				for(var i2 = 0, idx2 = bs[i], idy2 = idx2 + 1; i2 < histLen; i2++, idx2 += 2, idy2 += 2){
					posHist[idx2] = _positions[idx];
					posHist[idy2] = _positions[idy];
				}
				//clear this entry out
				pnu[i] = -1;
			}
		}
		else if(this.dimension === 3){
			for(var i = 0, idx = i * 3, idy = idx + 1, idz = idy + 1; i < _num; i++, idx += 3, idy += 3, idz += 3){
				bs[i] = i * histLen * 3;
				ts[i] = 0;
				bo[i] = i;
				//fill in the history with the initial position
				for(var i2 = 0, idx2 = bs[i], idy2 = idx2 + 1, idz2 = idy2 + 1; i2 < histLen; i2++, idx2 += 3, idy2 += 3, idz2 += 3){
					posHist[idx2] = _positions[idx];
					posHist[idy2] = _positions[idy];
					posHist[idz2] = _positions[idz];
				}
				//clear this entry out
				pnu[i] = -1;
			}
		}
		this.next = _num;
		this.last = this.next - 1;
		this.needsUpdateLatest = true;
		this.needsUpdateAll = true;
	},
	update: function(){
		
	},
	resize: function(_length, _skip){
		this.skip = Math.max(_skip, 1);
		this.historyLength = _length;
		
		
		//TODO: preserve the old data?
		//we'd need to mess with blockOwner and blockStart if we were to resize after there's already data in the simulation
		this.init();
	},
	findNextBlock: function(){
		//find the next available block
		for(var i = 0, len = this.capacity; i < len; i++){
			//TODO
		}
	}
};





















