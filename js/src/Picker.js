/**
*	Picker.js
*	6/4/2015
*	For selecting planets
*/

GRAV.Picker = function(_engine, _camera){
	this.engine = _engine;
	this.camera = _camera;
	
	this.size = 8;	//the 8 is arbitrary. I don't think it's possible to select more than 2 or 3 at once
	this.selected = new Int16Array(this.size);
	this.init();
};

GRAV.Picker.prototype = {
	constructor: GRAV.Picker,
	selected: undefined,
	camera: undefined,
	engine: undefined,
	pick: function(_x, _y){
		var e = this.camera.invMatrixWorld.elements;
		//clear out the selected array
		this.init();
		//convert the coordinates into model space
		var posx = e[0] * _x + e[3] * _y + e[6];
		var posy = e[1] * _x + e[4] * _y + e[7];
		//ask the engine if anyone's been selected
		var numSelected = this.engine.planetsAtPt(posx, posy, this.selected);
		return {
			num: numSelected,
			planets: Array.prototype.slice.call(this.selected)	//returns a normal array
		};
	},
	init: function(){
		for(var i = 0; i < this.size; i++){
			this.selected[i] = -1;
		}
	}
};